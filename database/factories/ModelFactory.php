<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(Encuestas\Models\User::class, function ($faker) {

    return [
        'first_name' => $faker->firstName,
    	'last_name' => $faker->lastName,
        'email' => $faker->unique()->email,
        'password' => bcrypt('password'),
        'remember_token' => null,
    ];

});

$factory->defineAs(Encuestas\Models\User::class, 'admin', function ($faker) use ($factory) {

    $user = $factory->raw(Encuestas\Models\User::class);

    return array_merge($user, [
        'user_type_id' => 1,
        'email' => 'admin@yopmail.com'
        ]);
});

$factory->defineAs(Encuestas\Models\User::class, 'suscriptor', function ($faker) use ($factory) {

    $user = $factory->raw(Encuestas\Models\User::class);

    return array_merge($user, [
        'email' => 'suscriptor' . $faker->unique()->regexify('[0-9]{1}') . '@yopmail.com',
        'user_type_id' => 2,
        'subscription_plan_id' => 1,
        'expire_on' => \Carbon\Carbon::now()->addDays(21900)
        ]);
});

$factory->defineAs(Encuestas\Models\User::class, 'usuario', function ($faker) use ($factory) {

    $user = $factory->raw(Encuestas\Models\User::class);

    $gender = $faker->boolean($chanceOfGettingTrue = 50);

    return array_merge($user, [
        'email' => 'usuario@yopmail.com',
    	'user_type_id' => 3,
    	'id_doc_number' => $faker->unique()->regexify('[0-9]{8}'),
    	'first_name' => $gender ? $faker->firstName('male') : $faker->firstName('female'),
    	'last_name' => $faker->lastName,
    	'phone' => 9 . $faker->unique()->randomNumber(8),
    	'birth_date' => $faker->dateTimeBetween($startDate = '-30 years', $endDate = 'now'),
    	'gender' => $gender,
    	'reward_points' => $faker->numberBetween($min = 0, $max = 200)
    	]);
});

$factory->define(Encuestas\Models\Survey::class, function ($faker) {
    
    $start_date = \Carbon\Carbon::instance( $faker->dateTimeBetween( $startDate = 'now', $endDate = '+5 days' ) )->startOfDay();;
    $end_date = ( $faker->boolean($chanceOfGettingTrue = 50) ) ? null : $start_date->copy()->addDays( $faker->numberBetween($min = 2, $max = 30) );

    return [
        'title' => '¿' . $faker->sentence( $faker->numberBetween($min = 4, $max = 10) ) . '?',
        'start_date' => $start_date->toDateTimeString(),
        'end_date' => is_null($end_date) ? null : $end_date->toDateTimeString(),
        'reward_points' => 1,
        'max_completed_per_survey' => $faker->randomElement( $array = array (100,250,500,1000) ),
        'survey_state_id' => 1
    ];

});

$factory->define(Encuestas\Models\Question::class, function ($faker) {

    return [
        'title' => $faker->sentence( $faker->numberBetween($min = 4, $max = 10) ),
        'help_text' => ( $faker->boolean($chanceOfGettingTrue = 50) ) ? null : $faker->sentence( $faker->numberBetween($min = 4, $max = 10) ),
        'question_type_id' => $faker->randomElement( $array = array (1,2) ),
    ];

});

$factory->define(Encuestas\Models\AnswerAlternative::class, function ($faker) {

    return [
        'description' => $faker->sentence( $faker->numberBetween($min = 1, $max = 4) )
    ];

});

$factory->defineAs(Encuestas\Models\AnswerAlternative::class, 'unica', function ($faker) use ($factory) {

    $answer_alternative = $factory->raw(Encuestas\Models\AnswerAlternative::class);

    return array_merge($answer_alternative, [
        'alternative_type_id' => 1
        ]);
});

$factory->defineAs(Encuestas\Models\AnswerAlternative::class, 'multiple', function ($faker) use ($factory) {

    $answer_alternative = $factory->raw(Encuestas\Models\AnswerAlternative::class);

    return array_merge($answer_alternative, [
        'alternative_type_id' => 2
        ]);
});

$factory->defineAs(Encuestas\Models\AnswerAlternative::class, 'libre', function ($faker) use ($factory) {

    $answer_alternative = $factory->raw(Encuestas\Models\AnswerAlternative::class);

    return array_merge($answer_alternative, [
        'alternative_type_id' => 3
        ]);
});

$factory->defineAs(Encuestas\Models\AnswerAlternative::class, 'rango', function ($faker) use ($factory) {

    $answer_alternative = $factory->raw(Encuestas\Models\AnswerAlternative::class);

    return array_merge($answer_alternative, [
        'alternative_type_id' => 2,
        'description' => $faker->sentence( $faker->numberBetween($min = 3, $max = 5) ),
        'min_value' => 1,
        'max_value' => $faker->numberBetween($min = 3, $max = 10),
        'min_tag' => $faker->sentence( 1 ),
        'max_tag' => $faker->sentence( 1 )
        ]);
});

$factory->defineAs(Encuestas\Models\AnswerAlternative::class, 'imagen', function ($faker) use ($factory) {

    $answer_alternative = $factory->raw(Encuestas\Models\AnswerAlternative::class);

    return array_merge($answer_alternative, [
        'alternative_type_id' => 1,
        'description' => $faker->sentence( $faker->numberBetween($min = 2, $max = 4) ),
        'img_url' => "http://lorempixel.com/40/40/"
        ]);
});