<?php

use Illuminate\Database\Seeder;
use Encuestas\Models\UserGroup;
class UserGroupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        # Linking user to group
        # usuario@yopmail.com
        UserGroup::create(['user_id' => 2, 'group_id' => 1]);
        UserGroup::create(['user_id' => 2, 'group_id' => 2]);

        #usuario2@yopmail.com
        UserGroup::create(['user_id' => 3, 'group_id' => 3]);
        UserGroup::create(['user_id' => 3, 'group_id' => 4]);

        #usuario3@yopmail.com
        UserGroup::create(['user_id' => 4, 'group_id' => 1]);
        UserGroup::create(['user_id' => 4, 'group_id' => 2]);
        UserGroup::create(['user_id' => 4, 'group_id' => 3]);

        #usuario4@yopmail.com
        UserGroup::create(['user_id' => 5, 'group_id' => 5]);
        UserGroup::create(['user_id' => 5, 'group_id' => 4]);
        UserGroup::create(['user_id' => 5, 'group_id' => 1]);
            
    }
}
