<?php

use Illuminate\Database\Seeder;

class SurveyTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('survey_types')->insert(
    		[
    			'id' => '1',
    			'name' => 'normal'
    		]
    	);
        DB::table('survey_types')->insert(
            [
                'id' => '2',
                'name' => 'plantilla'
            ]
        );
    }
}
