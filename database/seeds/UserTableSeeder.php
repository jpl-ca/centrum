<?php

use Illuminate\Database\Seeder;
use Encuestas\Models\User;
class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
        	"user_type_id" => 3,
        	"id_doc_number" => '00000002',
        	"first_name" => "Pepe",
        	"last_name" => "Ramirez",
        	"phone" => "999999999",
        	"birth_date" => "2016-01-01",
        	"gender" => 1,
        	"email" => "usuario2@yopmail.com",
        	"password" => bcrypt("password")
        	]);

        User::create([
        	"user_type_id" => 3,
        	"id_doc_number" => '00000003',
        	"first_name" => "María",
        	"last_name" => "Ramirez",
        	"phone" => "999999999",
        	"birth_date" => "2016-01-01",
        	"gender" => 1,
        	"email" => "usuario3@yopmail.com",
        	"password" => bcrypt("password")
        	]);

        User::create([
        	"user_type_id" => 3,
        	"id_doc_number" => '00000004',
        	"first_name" => "Juan",
        	"last_name" => "Ramirez",
        	"phone" => "999999999",
        	"birth_date" => "2016-01-01",
        	"gender" => 1,
        	"email" => "usuario4@yopmail.com",
        	"password" => bcrypt("password")
        	]);

        User::create([
        	"user_type_id" => 3,
        	"id_doc_number" => '00000005',
        	"first_name" => "Juan",
        	"last_name" => "Ramirez",
        	"phone" => "999999999",
        	"birth_date" => "2016-01-01",
        	"gender" => 1,
        	"email" => "usuario5@yopmail.com",
        	"password" => bcrypt("password")
        	]);
    }
}
