<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Encuestas\Models\Group;
use Encuestas\Models\UserGroup;
class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = \Faker\Factory::create();
        Model::unguard();
        factory(Encuestas\Models\User::class, 'admin', 1)->create();
        factory(Encuestas\Models\User::class, 'usuario', 1)->create();

        $this->call(SurveyTypeTableSeeder::class);
        $this->call(GroupTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(UserGroupTableSeeder::class);

        Model::reguard();
    }
}
