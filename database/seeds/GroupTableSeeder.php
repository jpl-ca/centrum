<?php

use Illuminate\Database\Seeder;
use Encuestas\Models\Group;
class GroupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Group::create(['name' => 'Grupo 1',
            'course_name' => "Nombre del curso",
            'program_type_name' => "Tipo de programa",
            'program_name' => "Programa",
            'phase_name' => "Ciclo I",
            'section_name' => 'Nombre de Sección',
            'teacher_name' => 'Nombre del profesor',
            'tutor_name' => "Nombre del tutor"
            ]);

         Group::create(['name' => 'Grupo 2',
            'course_name' => "Nombre del curso",
            'program_type_name' => "Tipo de programa",
            'program_name' => "Programa",
            'phase_name' => "Ciclo I",
            'section_name' => 'Nombre de Sección',
            'teacher_name' => 'Nombre del profesor',
            'tutor_name' => "Nombre del tutor"
            ]);

          Group::create(['name' => 'Grupo 3',
            'course_name' => "Nombre del curso",
            'program_type_name' => "Tipo de programa",
            'program_name' => "Programa",
            'phase_name' => "Ciclo I",
            'section_name' => 'Nombre de Sección',
            'teacher_name' => 'Nombre del profesor',
            'tutor_name' => "Nombre del tutor"
            ]);

          Group::create(['name' => 'Grupo 4',
            'course_name' => "Nombre del curso",
            'program_type_name' => "Tipo de programa",
            'program_name' => "Programa",
            'phase_name' => "Ciclo I",
            'section_name' => 'Nombre de Sección',
            'teacher_name' => 'Nombre del profesor',
            'tutor_name' => "Nombre del tutor"
            ]);

          Group::create(['name' => 'Grupo 5',
            'course_name' => "Nombre del curso",
            'program_type_name' => "Tipo de programa",
            'program_name' => "Programa",
            'phase_name' => "Ciclo I",
            'section_name' => 'Nombre de Sección',
            'teacher_name' => 'Nombre del profesor',
            'tutor_name' => "Nombre del tutor"
            ]);

          Group::create(['name' => 'Grupo 6',
            'course_name' => "Nombre del curso",
            'program_type_name' => "Tipo de programa",
            'program_name' => "Programa",
            'phase_name' => "Ciclo I",
            'section_name' => 'Nombre de Sección',
            'teacher_name' => 'Nombre del profesor',
            'tutor_name' => "Nombre del tutor"
            ]);
          Group::create(['name' => 'Grupo 7',
            'course_name' => "Nombre del curso",
            'program_type_name' => "Tipo de programa",
            'program_name' => "Programa",
            'phase_name' => "Ciclo I",
            'section_name' => 'Nombre de Sección',
            'teacher_name' => 'Nombre del profesor',
            'tutor_name' => "Nombre del tutor"
            ]);
    }
}
