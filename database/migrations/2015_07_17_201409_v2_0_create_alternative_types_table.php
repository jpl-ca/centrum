<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Eloquent\Model;

use Encuestas\Models\AlternativeType;

class V20CreateAlternativeTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alternative_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });

        Model::unguard();

        AlternativeType::create([ //radio
            'name' => 'única'
            ]);

        AlternativeType::create([ //checkbox
            'name' => 'múltiple'
            ]);

        AlternativeType::create([ //textfield
            'name' => 'libre'
            ]);

        Model::reguard();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('alternative_types');
    }
}
