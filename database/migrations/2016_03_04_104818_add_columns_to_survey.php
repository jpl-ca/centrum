<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToSurvey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('surveys', function(Blueprint $table){
            $table->integer('survey_type_id')->unsigned();
            $table->integer('group_id')->unsigned()->nullable();
            $table->foreign('survey_type_id')->references('id')->on('survey_types')->first();
            $table->foreign('group_id')->references('id')->on('groups')->first();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
