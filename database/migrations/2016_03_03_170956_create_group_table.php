<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('groups', function(Blueprint $table){
            $table->increments('id');
            $table->string('name');
            $table->string('course_name');
            $table->string('program_type_name');
            $table->string('program_name');
            $table->string('phase_name');
            $table->string('section_name');
            $table->string('teacher_name');
            $table->string('tutor_name');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('groups');
    }
}
