<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Eloquent\Model;

use Encuestas\Models\SubscriptionPlan;

class V20CreateSubscriptionPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscription_plans', function (Blueprint $table) {
            $table->increments('id');
            $table->string('description')->nullable();
            $table->decimal('price', 6, 2);
            $table->boolean('available');
            $table->integer('max_surveys');
            $table->integer('max_completed_per_survey');
            $table->integer('max_completed_surveys');
            $table->integer('expire_after');
            $table->timestamps();
        });

        Model::unguard();

        SubscriptionPlan::create([
            'description' => 'Vitalicio',
            'price' => 0,
            'available' => true,
            'max_surveys' => -1,
            'max_completed_per_survey' => -1,
            'max_completed_surveys' => -1,
            'expire_after' => 21900
            ]);

        Model::reguard();

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('subscription_plans');
    }
}
