<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class V20CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_type_id')->unsigned()->default(3);
            $table->string('id_doc_number')->unique()->nullable();
            $table->string('first_name');
            $table->string('last_name')->nullable();
            $table->string('phone')->nullable();
            $table->dateTime('birth_date')->nullable();
            $table->boolean('gender')->default(0); //0: femenino, 1: masculino 
            $table->string('email')->unique();
            $table->string('password', 60);
            $table->string('last_position')->nullable();
            $table->integer('reward_points')->nullable();
            $table->integer('subscription_plan_id')->unsigned()->nullable();
            $table->dateTime('expire_on')->nullable();
            $table->boolean('has_trial_period')->default(true);
            $table->rememberToken();
            $table->timestamps();

            $table->foreign('user_type_id')->references('id')->on('user_types');
            $table->foreign('subscription_plan_id')->references('id')->on('subscription_plans');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
