<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class V20CreateSurveysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('surveys', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->string('description')->nullable();
            $table->dateTime('start_date')->nullable();
            $table->dateTime('end_date')->nullable();
            $table->integer('reward_points');
            $table->integer('max_completed_per_survey');
            $table->integer('belongs_to_user_id')->unsigned()->nullable();
            $table->integer('survey_state_id')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('belongs_to_user_id')->references('id')->on('users');
            $table->foreign('survey_state_id')->references('id')->on('survey_states');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('surveys');
    }
}
