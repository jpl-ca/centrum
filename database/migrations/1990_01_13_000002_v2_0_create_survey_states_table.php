<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Eloquent\Model;

use Encuestas\Models\SurveyState;

class V20CreateSurveyStatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('survey_states', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });

        Model::unguard();

        SurveyState::create(['name' => 'Pendiente']);
        SurveyState::create(['name' => 'Activo']); 
        SurveyState::create(['name' => 'Finalizado']);
        SurveyState::create(['name' => 'Inactivo']);

        Model::reguard();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('survey_states');
    }
}
