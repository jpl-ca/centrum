@extends('app.layouts.master')

@section('master-container')
    @section('title', 'ng-app="app"')
    @section('js')
    <script src="{{ asset('js/scripts/app.js')}}"></script>
    <script src="{{ asset('js/scripts/controllers/controller.js')}}"></script>
    <script src="http://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.13.0.js"></script>
    @stop
    <div id="wrapper">
    
        @include('app.layouts.fragments.sidebar')

        <!-- Page Content -->
        <div id="page-wrapper">
          <br>
          <div class="card">
            <div class="container-fluid">

                @include('app.layouts.fragments.alerts')

                <div class="row">
                    <div class="col-lg-12"  ng-controller="appController">
                        <h2 ng-bind="titulo"></h2>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>

                <div class="row">
                    @yield('base-container')
                </div>

                
                <div class="row">
                    <div class="col-sm-12" >
                        <ul class="nav nav-tabs" ng-controller="appController">
                          <li ng-class="{ active : url == 'e'  }" ><a data-toggle="tab" href="#e">Encuestas</a></li>
                          <li ng-class="{ active : url == 'p'  }"><a data-toggle="tab" href="#p">Plantillas</a></li>
                        </ul>
                        <div class="tab-content" ng-controller="appController">
                          <div id="e" class="tab-pane fade in" ng-class="{ active : url == 'e'  }">
                            <br>
                            <div class="row">
                                <div class="col-sm-12 margin-bottom-20">
                                    <a href="{{ route('app.surveys.create') }}" class="btn btncrear pull-right">Crear nueva encuesta</a>
                                </div>
                            </div>
                            <br>
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th style="width:65%;">Asignatura</th>
                                            <th style="width:10%;text-align:center;">Inicio</th>
                                            <th style="width:10%;text-align:center;">Avances</th>
                                            <th style="width:15%;text-align:center;">Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr  ng-repeat="survey in surveys">
                                             <td>@{{survey.group.name}} </td>
                                             <td style="text-align:center;">@{{survey.start_date | date_format}}</td>
                                             <td style="text-align:center;">@{{survey.current_completed_surveys}}/@{{survey.max_completed_per_survey}}</td>
                                             <td style="text-align: right;">
                                             <a href="{{ route('app.surveys.aprobar')}}/@{{survey.id}}" class="accion"  tooltip="aprobar respuestas"><i class="fa fa-check"></i></a>
                                             <a href="{{ url('')}}/admin/surveys/@{{survey.id}}/editar" class="accion"  tooltip="editar"><i class="fa fa-pencil"></i></a>
                                             <a ng-click="desactiva(survey.id)" ng-if="survey.survey_state_id == 2" class="accion"  tooltip="detener" style="text-decoration: none;cursor:pointer;">
                                             <i class="fa fa-pause"></i>
                                             </a>
                                             <a ng-click="activa(survey.id)" ng-if="survey.survey_state_id == 4" class="accion"  tooltip="activar" style="text-decoration: none;cursor:pointer;"> 
                                             <i class="fa fa-play"></i>
                                             </a>
                                             <a href="{{ url('')}}/admin/reports/survey/@{{survey.id}}" class="accion"  tooltip="reportes"><i class="fa fa-bar-chart"></i></a>
                                             <a ng-click="delete_survey(survey.id, $index)" class="accion" tooltip="eliminar" style="cursor:pointer;"><i class="fa fa-times-circle"></i></a>
                                             </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                          </div>

                          <div id="p" class="tab-pane fade in" ng-class="{ active : url == 'p'  }">
                            <br>
                            <div class="row">
                                <div class="col-sm-12 margin-bottom-20">
                                    <a href="{{ route('app.templates.create') }}" class="btn btncrear pull-right">Crear nueva plantilla</a>
                                </div>
                            </div>
                            <br>    
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th style="width:65%;">Nombre</th>
                                            <th style="width:15%;text-align:right;">Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <tr ng-repeat="template in plantillas">
                                             <td>@{{template.title}}</td>
                                             <td style="text-align: right;">
                                             <a href="{{ route('app.surveys.edit') }}/@{{template.id}}" class="accion" tooltip="editar"><i class="fa fa-pencil"></i></a>
                                             <a ng-click="delete_template(template.id, $index)" class="accion" tooltip="eliminar" tooltip-placement="bottom" tooltip-append-to-body="true"><i class="fa fa-times-circle"></i></a>
                                             </td>
                                         </tr>
                                    </tbody>
                                </table>
                            </div>
                          </div>
                        </div>                      

                    </div>
                </div>

                <!-- /.row -->
            </div>
          </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
@stop

