@extends('app.layouts.base')

@section('base-container')
	<div class="col-sm-12">
		<form class="form-horizontal" action="{{ route('app.users.update-profile') }}" method="POST">
			{!! csrf_field() !!}
			<div class="row">
				<div class="col-sm-6">

		        	<h2 class="section-header">Información Personal</h2>
					<div class="form-group">
						<label class="col-sm-4 control-label">Nombres</label>
						<div class="col-sm-8">
							<p class="form-control-static">{{ auth()->user()->first_name }}</p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-4 control-label">Apellidos</label>
						<div class="col-sm-8">
							<p class="form-control-static">{{ auth()->user()->last_name }}</p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-4 control-label">Email</label>
						<div class="col-sm-8">
							<p class="form-control-static">{{ auth()->user()->email }}</p>
						</div>
					</div>
					<div class="form-group">
						<label for="password" class="col-sm-4 control-label">Contraseña</label>
						<div class="col-sm-8">
							<input type="password" class="form-control" name="password" placeholder="contraseña">
						</div>
					</div>

				</div>
				<div class="col-sm-6">

		        	<h2 class="section-header text-right">Información de Suscripción</h2>
					<div class="form-group">
						<label class="col-sm-6 control-label">Tipo de plan</label>
						<div class="col-sm-6">
							<p class="form-control-static">{{ auth()->user()->subscription_plan->description }}</p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-6 control-label">Valido desde</label>
						<div class="col-sm-6">
							<p class="form-control-static">{{ (new \Carbon\Carbon(auth()->user()->current_plan()->start_date))->format('Y-m-d') }}</p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-6 control-label">Valido hasta</label>
						<div class="col-sm-6">
							<p class="form-control-static">{{ auth()->user()->subscription_plan_id == 1 ? '-' : auth()->user()->expiration_date() }}</p>
						</div>
					</div>
					@if( is_null(auth()->user()->current_plan()->available_completed_surveys) )
					<div class="form-group">
						<label class="col-sm-6 control-label">Encuestas disponibles</label>
						<div class="col-sm-6">
							<p class="form-control-static">{{ auth()->user()->current_plan()->available_surveys}}</p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-6 control-label">Muestra por encuesta</label>
						<div class="col-sm-6">
							<p class="form-control-static">{{ auth()->user()->current_plan()->max_completed_per_survey }}</p>
						</div>
					</div>
					@else
					<div class="form-group">
						<label class="col-sm-6 control-label">Tamaño de muestra disponible</label>
						<div class="col-sm-6">
							<p class="form-control-static">{{ auth()->user()->current_plan()->available_completed_surveys < 0 ? 'Ilimitado' : auth()->user()->current_plan()->available_completed_surveys }}</p>
						</div>
					</div>
					@endif

				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<button type="submit" class="btn btn-info btn-lg center-block">Guardar</button>
				</div>
			</div>
		</form>
	</div>

@stop