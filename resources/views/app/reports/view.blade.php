@extends('app.layouts.master')

@section('master-container')
    @section('title', 'ng-app="app"')
    @section('js')
    <script src="{{ asset('js/scripts/app.js')}}"></script>
    <script src="{{ asset('js/scripts/controllers/controller_reports.js')}}"></script>
    <script src="http://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.13.0.js"></script>
    <link rel="stylesheet" href="{{ asset('vendor/sb-admin-2/bower_components/angular-chart.js/dist/angular-chart.css')}}">
    @stop
    <div id="wrapper">
    
        @include('app.layouts.fragments.sidebar')

        <!-- Page Content -->
        <div id="page-wrapper">
          <br>
          <div class="card">
            <div class="container-fluid" ng-controller="appController"  ng-init="survey_id={{$survey->id}}">

                @include('app.layouts.fragments.alerts')
                <div class="row">
                        <div class="col-lg-12">
                           
                            <a href="{{ url('')}}/mis-encuestas" class="btn btn-danger pull-right">
                            <i class="fa fa-chevron-left"></i> 
                            Volver</a href="{{ url('')}}/mis-encuestas">
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                <div class="row">
                    <div class="col-lg-12">
                        <h2 align="center">Consolidado de Encuesta: @{{titulo}}</h2>
                        <br>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <div class="row">
                    @yield('base-container')
                </div>

                  <div class="container">
                    <div class="row">
                      <div class="col-md-2"></div>
                      <div class="col-md-8">
                          <canvas id="line" class="chart chart-line" chart-data="data"
                          chart-labels="labels" chart-legend="true" chart-series="series"
                          chart-click="onClick" >
                          </canvas>        
                      </div>
                      <div class="col-md-2"></div>
                    </div>
                    <div class="row">
                    <br>
                      <div class="col-md-2"></div>
                      <div class="col-sm-8">
                          <a href="/admin/reports/survey/graphic-a/{{$survey->id}}?format=xls" download class="btn btn-success pull-right"><i class="fa fa-download"></i> Exportar a Excel</a>      
                      </div>
                      <div class="col-md-2"></div>
                    </div>
                    <div class="row">
                    <br>
                      <div class="col-md-2"></div>
                      <div class="col-md-8">
                      <div class="container"><h3 for="">Preguntas</h3></div>
                        <table class="table">
                          <tbody>  
                            <tr ng-repeat="small in question_smalls">
                              <td><label>Pregunta @{{$index + 1}}:</label> @{{small}} </td>
                            </tr>                            
                          </tbody>  
                        </table>                              
                      </div>
                      <div class="col-md-2"></div>
                    </div>                    
                    <br>
                  </div>

                <!-- /.row -->
            </div>
          </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
@stop

