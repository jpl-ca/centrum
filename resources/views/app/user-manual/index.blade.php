@extends('app.layouts.base')

@section('base-container')
	
	@foreach($manual as $indication)
    <div class="row margin-bottom-30" id="{{ $indication['id'] }}">
        <div class="col-sm-4">
            <h3 class="sub-legend">{{ $indication['title'] }}</h3>
				<ol class="iris-ol">
					@foreach($indication['steps'] as $step)
				    <li>{{ $step }}</li>
				    @endforeach
				</ol>
        </div>
		<div class="col-sm-8">
			{{--*/ $image = $indication['image']; /*--}}
            <img class="img-responsive" src="{{ asset("images/manual/$image.png") }}" alt="imagen detalle">
		</div>
	</div>
	@endforeach

@stop