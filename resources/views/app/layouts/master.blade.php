<!DOCTYPE html>
<html lang="es">

<head>

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    
    <title>{{ $pagename or '' }} - Encuestas</title>

    @include('app.layouts.fragments.import-css')

</head>

<body @yield('title')>
    
    @yield('master-container')

    @include('app.layouts.fragments.import-js')

    @yield('js')
</body>

</html>
