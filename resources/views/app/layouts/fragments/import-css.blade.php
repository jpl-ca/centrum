    <!-- for angular sweet alert-->

    <link href="{{ asset('vendor/sb-admin-2/bower_components/sweetalert/dist/sweetalert.css')}}" rel="stylesheet">

    <!-- Bootstrap Core CSS -->
    <link href="{{ asset('vendor/sb-admin-2/bower_components/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="{{ asset('vendor/sb-admin-2/bower_components/metisMenu/dist/metisMenu.min.css')}}" rel="stylesheet">

    <!-- Smoke CSS -->
    <link href="{{ asset('vendor/sb-admin-2/bower_components/smoke/dist/css/smoke.css')}}" rel="stylesheet">

    <!-- Bootstrap Datetime Picker CSS -->
    <link href="{{ asset('vendor/sb-admin-2/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet">

    <!-- Bootstrap Touchspin CSS -->
    <link href="{{ asset('vendor/sb-admin-2/bower_components/bootstrap-touchspin/src/jquery.bootstrap-touchspin.css')}}" rel="stylesheet">

    <!-- Bootstrap Switch CSS -->
    <link href="{{ asset('vendor/sb-admin-2/bower_components/bootstrap-switch/css/bootstrap3/bootstrap-switch.min.css')}}" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="{{ asset('vendor/sb-admin-2/bower_components/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">

    <!-- Encuestas CSS -->
    <link href="{{ asset('css/encuestas.css')}}" rel="stylesheet">

    <link href="{{ asset('vendor/sb-admin-2/bower_components/ngCheckbox/ngCheckbox.css')}}" rel="stylesheet">
    
    <!-- Custom CSS -->
    <link href="{{ asset('vendor/sb-admin-2/dist/css/sb-admin-2.css')}}" rel="stylesheet">
    
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js'}}"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js'}}"></script>
    <![endif]-->

    <!-- for data timepicker bootstrap angular-->
    <link href="{{ asset('vendor/sb-admin-2/bower_components/angular-bootstrap-datetimepicker/src/css/datetimepicker.css')}}" rel="stylesheet">

