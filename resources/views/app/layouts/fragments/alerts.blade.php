	<div class="margin-top-20">
        @if(Session::has('success'))
        <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
           	<strong>¡Correcto!</strong> {{Session::get('success')}}
        </div>
        @endif
        @if(Session::has('info'))
        <div class="alert alert-info alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <strong>¡Atención!</strong> {{Session::get('info')}}
        </div>
        @endif
        @if(Session::has('warning'))
        <div class="alert alert-warning alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <strong>¡Advertencia!</strong> {{Session::get('warning')}}
        </div>
        @endif
        @if(Session::has('error'))
        <div class="alert alert-danger alert-dismissable" style="position: fixed;
            width: 50%;
            z-index: 100;
            margin-top: 36%;">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <strong>¡Hubo un error!</strong> {{Session::get('error')}}
        </div>
        @endif
        @if(Session::has('validation_errors'))
        <div class="alert alert-danger alert-dismissable" style="position: fixed;
            width: 50%;
            z-index: 100;
            margin-top: 36%;">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <strong>¡Hubieron algunos errores!</strong>
            <ul style="margin-left: 25px;">
                @foreach (Session::get('validation_errors') as $validation_error)
                    <li>{{$validation_error}}</li>
                @endforeach
            </ul>
        </div>
        @endif

        <!--Sticky alerts-->
        @if(Session::has('fixed_success'))
        <div class="alert alert-success alert-dismissable">
           	<strong>¡Correcto!</strong> {{Session::get('fixed_success')}}
        </div>
        @endif
        @if(Session::has('fixed_info'))
        <div class="alert alert-info alert-dismissable">
            <strong>¡Atención!</strong> {{Session::get('fixed_info')}}
        </div>
        @endif
        @if(Session::has('fixed_warning'))
        <div class="alert alert-warning alert-dismissable">
            <strong>¡Advertencia!</strong> {{Session::get('fixed_warning')}}
        </div>
        @endif
        @if(Session::has('fixed_error'))
        <div class="alert alert-danger alert-dismissable">
            <strong>¡Hubo un error!</strong> {{Session::get('fixed_error')}}
        </div>
        @endif
    </div>