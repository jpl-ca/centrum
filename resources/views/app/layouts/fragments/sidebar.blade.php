        <!-- Navigation -->
        
        <nav class="navbar navbar-default navbar-static-top navlogo" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <!-- <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button> -->
                <a class="navbar-brand" href="{{ route('app.surveys.index') }}"><img src="{{ asset('images/logo/logo.png')}}" class="img img-responsive logocen" alt="logo centrum"></a>
            </div>

            @include('app.layouts.fragments.topbar')

            <!-- <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse" style="border-color: #58595B;background: #58595B;">
                    <ul class="nav" id="side-menu">
                        {{--
                        <li>
                            <a href="{{ route('dashboard') }}"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>
                        --}}
                        <li>
                            <a href="{{ route('app.surveys.index') }}" style="padding: 25px 15px;" class="opcmenu"><i class="fa fa-bar-chart-o fa-fw"></i> Mis encuestas</a>
                        </li>
                        {{--*/
                            $manual = Encuestas\Libraries\UserManual::getManual();
                        /*--}}
                        <li class="">
                            <a href="javascript:void(0)" style="padding: 25px 15px;" class="opcmenu"><i class="fa fa-files-o fa-fw"></i> Manual de usuario<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                @foreach($manual as $indication)
                                <li>
                                    <a class="opcmenu" href="{{ url('manual-de-usuario').'#'.$indication['id'] }}">{{ $indication['title'] }}</a>
                                </li>
                                @endforeach
                            </ul>
                        </li>
                        
                    </ul>
                </div>
            </div> -->
        </nav>