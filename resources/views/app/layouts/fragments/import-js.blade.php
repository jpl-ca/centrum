    <!-- jQuery -->
    <script src="{{ asset('vendor/sb-admin-2/bower_components/jquery/dist/jquery.min.js')}}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{ asset('vendor/sb-admin-2/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="{{ asset('vendor/sb-admin-2/bower_components/metisMenu/dist/metisMenu.min.js')}}"></script>

    <!-- Custom Theme JavaScript -->
    <script src="{{ asset('vendor/sb-admin-2/dist/js/sb-admin-2.js')}}"></script>
    

    <!-- Smoke JavaScript -->
    <script src="{{ asset('vendor/sb-admin-2/bower_components/smoke/dist/js/smoke.js')}}"></script>
    <script src="{{ asset('vendor/sb-admin-2/bower_components/smoke/dist/lang/es.js')}}"></script>

    <!-- Highcharts JavaScript -->
    <script type="text/javascript" src="{{ asset('vendor/sb-admin-2/bower_components/highcharts/highcharts.js')}}"></script>

    <!-- Bootstrap Datetime Picker JavaScript -->
	<script type="text/javascript" src="{{ asset('vendor/sb-admin-2/bower_components/moment/min/moment.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('vendor/sb-admin-2/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js')}}"></script>

    <!-- Bootstrap Touchspin JavaScript -->
    <script type="text/javascript" src="{{ asset('vendor/sb-admin-2/bower_components/bootstrap-touchspin/src/jquery.bootstrap-touchspin.js')}}"></script>

    <!-- Bootstrap Switch JavaScript -->
	<script type="text/javascript" src="{{ asset('vendor/sb-admin-2/bower_components/bootstrap-switch/js/bootstrap-switch.min.js')}}"></script>


    <!-- angular components -->
    <script src="{{ asset('vendor/sb-admin-2/bower_components/angular/angular.min.js')}}"></script>
    <script src="{{ asset('vendor/sb-admin-2/bower_components/angular-resource/angular-resource.min.js')}}"></script>
    <script src="{{ asset('vendor/sb-admin-2/bower_components/angular-bootstrap-datetimepicker/src/js/datetimepicker.js')}}"></script>
    <script src="{{ asset('vendor/sb-admin-2/bower_components/angular-bootstrap-datetimepicker/src/js/datetimepicker.templates.js')}}"></script>

    <!-- angular sweet alert -->
    <script src="{{ asset('vendor/sb-admin-2/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{ asset('vendor/sb-admin-2/bower_components/angular-sweetalert/dist/ngSweetAlert.js')}}"></script>
   

    
    <script type="text/javascript" src="{{ asset('vendor/sb-admin-2/bower_components/ngCheckbox/ngCheckbox.js')}}"></script>


    <script src="{{ asset('vendor/sb-admin-2/bower_components/Chart.js/Chart.js')}}"></script>
    <script src="{{ asset('vendor/sb-admin-2/bower_components/angular-chart.js/dist/angular-chart.js')}}"></script>