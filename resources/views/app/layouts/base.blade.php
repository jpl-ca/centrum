@extends('app.layouts.master')

@section('master-container')

    <div id="wrapper">

        @include('app.layouts.fragments.sidebar')

        <!-- Page Content -->
        <div id="page-wrapper">            
                <div class="container-fluid">
                    
                    @include('app.layouts.fragments.alerts')

                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">{{ $pagetitle or '' }} <small>{{ $subtitle or '' }}</small> 
                            <a href="{{ url('')}}/mis-encuestas" class="btn btn-danger pull-right">
                            <i class="fa fa-chevron-left"></i> 
                            Volver</a href="{{ url('')}}/mis-encuestas">
                            </h1>

                        </div>
                        <!-- /.col-lg-12 -->
                    </div>

                    <div class="row">
                        @yield('base-container')
                    </div>
                    <!-- /.row -->
                </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
    <!-- footer -->
    
@stop