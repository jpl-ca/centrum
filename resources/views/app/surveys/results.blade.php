@extends('app.layouts.base')

@section('base-container')
    <div class="col sm-12">
        <h4 class="section-header margin-bottom-15">Datos de la encuesta</h4>
        <div class="row margin-bottom-30">
            <div class="col-sm-12">
                <div class="form-group">
                    <div class="col-sm-2">
                        <p class="form-control-static"><strong>Estado</strong></p>
                    </div>
                    <div class="col-sm-10">
                        <p class="form-control-static">{{ $survey->survey_state->name }}</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group">
                    <div class="col-sm-2">
                        <p class="form-control-static"><strong>Respuestas</strong></p>
                    </div>
                    <div class="col-sm-10">
                        <p class="form-control-static">{{ $survey->current_completed_surveys .'/'. $survey->max_completed_per_survey }}</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group">
                    <div class="col-sm-2">
                        <p class="form-control-static"><strong>Fecha de inicio</strong></p>
                    </div>
                    <div class="col-sm-10">
                        <p class="form-control-static">{{$survey->asDate('start_date')}}</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group">
                    <div class="col-sm-2">
                        <p class="form-control-static"><strong>Fecha de fin</strong></p>
                    </div>
                    <div class="col-sm-10">
                        <p class="form-control-static">{{$survey->asDate('end_date')}}</p>
                    </div>
                </div>
            </div>
        </div>
        <h4 class="section-header">Resultados</h4> 
    </div>
    
    @foreach($chart->getDivs() as $question_id => $div)
    <div class="row">
        <div class="col-sm-12">
            <!-- Button trigger modal -->
            <a href="" class="btn btn-info btn-xs pull-left margin-bottom-20" data-toggle="modal" data-target="#modal_{{ $question_id }}">ver detalle</a>
            <!-- Modal -->
            <div class="modal fade" id="modal_{{ $question_id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-body margin-top-20">
                            <strong>{{ $results[$question_id]['question'] }}</strong>
                            @foreach( $results[$question_id]['alternatives'] as $alternative )
                            {{--*/
                                $total = $results[$question_id]['total_answers'];
                                $parcial = $total == 0 ? 0 : round( $alternative['y'] / $total *100, 1 );
                            /*--}}
                            <div class="radio">
                                <label>
                                    <input type="radio" name="radio{{ $results[$question_id]['question'] }}">
                                    {{ $alternative['name'] }}
                                </label>
                            </div>
                            <div class="progress">
                                <div class="progress-bar" role="progressbar" aria-valuenow="{{ $parcial }}" aria-valuemin="0" aria-valuemax="{{ $total }}" style="width: {{ $parcial }}%;">
                                {{ $parcial }}%
                                </div>
                            </div>
                            @endforeach
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">cerrar</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
            <div class="row">
                <div class="col-sm-12">
                	{!! $div !!}
                </div>    
            </div>    
        </div>    
    </div>
    @endforeach

@stop

@section('js')
    {!! $chart->getJS() !!}
@stop