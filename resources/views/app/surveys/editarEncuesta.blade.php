@extends('app.layouts.base')

@section('title', 'ng-app="app"')
@section('base-container')
    @section('js')
    <script src="{{ asset('js/scripts/app.js')}}"></script>
    <script src="{{ asset('js/scripts/controllers/controller_edit_survey.js')}}"></script>
    <script src="http://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.13.0.js"></script>
    @stop
    

    <div class="row" >
        <div class="col-sm-12">

			<div class="panel panel-default">
                <div class="panel-heading">
                    Edición de Encuesta
                </div>
				<div class="container">
                <br>
				        <form ng-controller="appController" role="form"  ng-init="survey_id={{$survey->id}}">
                  <div class="col-md-6">
                      <div class="form-group">
                        <label for="sel1">Plantilla:</label>
                        <input type="text" class="form-control" ng-model="survey.title" disabled>
                      </div>
                      <div class="form-group">
                        <label for="sel2">Grupo:</label>
                        <input type="text" class="form-control" ng-model="survey.group.name" disabled>
                      </div>
                      <div class="form-group">
                        <label for="tprog">Tipo de Programa:</label>
                        <input type="text" class="form-control" id="tprog" name="tprog" disabled value="@{{survey.group.program_type_name}}">
                      </div>
                      <div class="form-group">
                        <label for="prog">Programa:</label>
                        <input type="text" class="form-control" id="prog" name="programa" disabled value="@{{survey.group.program_name}}">
                      </div>
                      <div class="form-group">
                        <label for="cicl">Ciclo:</label>
                        <input type="text" class="form-control" id="cicl" name="ciclo"  disabled value="@{{survey.group.phase_name}}">
                      </div>                                           
                  </div>
                  <div class="col-md-6">
                      <div class="form-group">
                        <label for="curs">Curso:</label>
                        <input type="text" class="form-control" id="curs" name="curso" disabled value="@{{survey.group.course_name}}">
                      </div>
                      <div class="form-group">
                        <label for="secc">Sección:</label>
                        <input type="text" class="form-control" id="secc" name="seccion" disabled value="@{{survey.group.section_name}}">
                      </div>
                      <div class="form-group">
                        <label for="fchen">Fecha de Encuesta:</label>
                        <div class="dropdown">
                            <a class="dropdown-toggle" id="dropdown1" role="button" data-toggle="dropdown" data-target="#"
                               href="#">
                                <div class="input-group">
                                    <input style="background:white;" type="text" class="form-control" disabled data-ng-model="survey.start_date" datepicker-popup="dd-MM-yyyy">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                </div>
                            </a>
                            <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                                <datepicker data-ng-model="survey.start_date" 
                                                data-datetimepicker-config="{ dropdownSelector: '#dropdown1' }"></datepicker>
                            </ul>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="prof">Profesor:</label>
                        <input type="text" class="form-control" id="prof" name="tutor" disabled value="@{{survey.group.teacher_name}}">
                      </div>
                      <div class="form-group">
                        <label for="tut">Tutor:</label>
                        <input type="text" class="form-control" id="tut" name="tutor" disabled value="@{{survey.group.tutor_name}}">
                      </div>                                    
                    </div> 
                    <div class="col-md-12">
                        <div class="col-md-6">
                          <div class="form-group">
                            <a href="{{ route('app.surveys.index') }}" class="btn btn-danger pull-left">Cancelar</a>
                          </div>  
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <button type="submit" ng-click="add_survey()" class="btn btn-success pull-right" id="submit">Actualizar</button>
                          </div>  
                        </div>
                    </div>             		
	              </form>
                </div>
                <br>
            </div>

        </div>
    </div>    
@stop
 