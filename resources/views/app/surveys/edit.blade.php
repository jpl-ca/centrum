@extends('app.layouts.base')

@section('title', 'ng-app="app"')
@section('base-container')
    @section('js')
    <script src="{{ asset('js/scripts/app.js')}}"></script>
    <script src="{{ asset('js/scripts/controllers/controller_edit_template.js')}}"></script>
    <script src="http://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.13.0.js"></script>
    @stop
    

    <div class="row">
      <div class="col-sm-12">
        <div class="panel panel-default">
          <div class="panel-heading">
                    Editar Plantilla
                </div>
          <br />
          <form ng-controller="appController" role="form" ng-init="survey_id={{$survey->id}}">
            <div class="container panel panel-default">
              <br />
              <div class="col-md-12">
                <div class="form-group">
                  <label for="tprog">Título:</label>
                  <input type="text" required class="form-control" id="title_template" name="title_template" ng-model="template.title" />
                </div>
                <div class="form-group">
                  <label for="tprog">Descripcición:</label>
                  <input type="text"  class="form-control" id="descrip_template" name="descrip_template" ng-model="template.description" />
                </div>
                <div class="form-group" style="overflow: hidden;">
                          <label for="tprog">Escalas:</label>
                          <div class="form-inline col-md-12">
                            <div class="col-md-2 input-group" style="margin-left:25px;margin-top: 15px;margin-bottom: 15px;">
                              <span class="input-group-addon" style="cursor: pointer;">
                                                  a)
                                               </span>
                              <input type="text" disabled value="Nunca" class="form-control" id="escala" name="escala" />
                            </div>
                            <div class="col-md-2 input-group" style="margin-left:25px;margin-top: 15px;margin-bottom: 15px;">
                              <span class="input-group-addon" style="cursor: pointer;">
                                                  b)
                                               </span>
                              <input type="text" disabled value="Pocas Veces" class="form-control" id="escala" name="escala" />
                            </div>
                            <div class="col-md-2 input-group" style="margin-left:25px;margin-top: 15px;margin-bottom: 15px;">
                              <span class="input-group-addon" style="cursor: pointer;">
                                                  c)
                                               </span>
                              <input type="text" disabled value="Algunas Veces" class="form-control" id="escala" name="escala" />
                            </div>
                            <div class="col-md-2 input-group" style="margin-left:25px;margin-top: 15px;margin-bottom: 15px;">
                              <span class="input-group-addon" style="cursor: pointer;">
                                                  d)
                                               </span>
                              <input type="text" disabled value="Casi Siempre" class="form-control" id="escala" name="escala" />
                            </div>
                            <div class="col-md-2 input-group" style="margin-left:25px;margin-top: 15px;margin-bottom: 15px;">
                              <span class="input-group-addon" style="cursor: pointer;">
                                                  e)
                                               </span>
                              <input type="text" disabled value="Siempre" class="form-control" id="escala" name="escala" />
                            </div>
                          </div>
                          <br />
                        </div>
                <div>
                  <div ng-repeat="section in template.sections" data-sec="@{{$index}}">
                    <div class="modal-content" style="-webkit-box-shadow:none;box-shadow:none;">
                      <div class="modal-header">
                        <button style="margin-top: -10px;" type="button" class="close" ng-click="Remove_Section($index)">×</button>
                      </div>
                      <div class="modal-body" style="overflow: hidden;">
                        <div class="form-group">
                          <label for="tprog">Título de Sección:</label>
                          <input type="text" required class="form-control" id="title_section" name="title_section"  ng-model="section.name" />
                        </div>
                        <div class="form-group col-md-12 col-xs-12 ">
                          <label for="tprog">Preguntas:</label>
                          <div class="col-md-12 col-xs-12 form-inline" ng-repeat="question in section.questions" style="margin-bottom: 15px;">
                            <div class="input-group col-md-10 col-xs-10">
                              <input ng-model="question.title" required type="text" class="form-control" id="pregunta" name="pregunta" />
                              <span class="input-group-addon" style="cursor: pointer;width: 50px;" ng-click="Remove_Question($parent.$index,$index)">
                                <i class="fa fa-times"></i>
                              </span>
                            </div>
                            <div class="col-md-2 col-xs-2">
                              <label for="">Peso: @{{question.weight}}</label>  
                              <select class="form-control" id="sel1"  ng-model="question.weight">
                              <option value="1">1</option>
                              <option value="2">2</option>
                              <option value="3">3</option>
                              <option value="4">4</option>
                              <option value="5">5</option>
                              <option value="6">6</option>
                              <option value="7">7</option>
                              <option value="8">8</option>
                              <option value="9">9</option>
                              <option value="10">10</option>
                              </select>
                            </div>
                          </div>
                          <br />
                          <button ng-click="Add_Question($index)" class="btn btn-success">Agregar Pregunta</button>
                        </div>
                      </div>
                    </div>
                    <br />
                  </div>
                  <button ng-click="Add_Section()" class="btn btn-info">Agregar Sección</button>
                </div>
                <br />
              </div>
            </div>
            <div class="container">
              <div class="col-md-12">
                <div class="col-md-6">
                  <a href="{{ url('')}}/mis-encuestas?tab=p" class="btn btn-danger pull-left">Cancelar</a>
                </div>
                <div class="col-md-6">
                  <button class="btn btn-success pull-right" ng-click="add_template()">Actualizar</button>
                </div>
              </div>
            </div>
          </form>
          <br />
        </div>
      </div>
    </div>
@stop
 