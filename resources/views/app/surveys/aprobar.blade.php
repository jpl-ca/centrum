@extends('app.layouts.base')

@section('title', 'ng-app="app"')
@section('base-container')
    @section('js')    
    <script src="{{ asset('js/scripts/app.js')}}"></script>
    <script src="{{ asset('js/scripts/controllers/controller_aprobar_surveys.js')}}"></script>
    <script src="http://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.13.0.js"></script>
    @stop

    <div class="row">
      <div class="col-sm-12">
        <div class="panel panel-default">
          <div class="panel-heading">
                    Aprobar Respuestas 
                </div>
          <br />          
          <form ng-controller="appController" role="form"  ng-init="survey_id={{$survey->id}}"> 
            <div class="container panel panel-default">
              <br />
              <div class="col-md-12" ng-repeat="question in survey.questions">
                    <div  class="modal-content" style="margin-bottom:15px;-webkit-box-shadow:none;box-shadow:none;">                      
                      <div class="modal-body" style="overflow: hidden;">
                         <label>• @{{question.title}}</label>
                         <div class="form-group col-md-12" ng-repeat="answer in question.answers">
                            <div class="col-md-1 checkbox" style="width: 3%;">   
                              <label for="">
                              <input type="checkbox" ng-model="answer.enabled" ng-change="update(answer, false)">
                              </label>
                            </div>
                            <div class="col-md-11"> 
                              <label for="">Respuesta: @{{answer.alternative_title}} (@{{answer.answer_alternative.value}})</label> 
                              <textarea style="resize: none;" disabled class="form-control">@{{answer.comment}}
                              </textarea>                              
                            </div>
                         </div>
                      </div>
                    </div>
                <br>
              </div>
            </div>
            <div class="container">
              <div class="col-md-12">
                <div class="col-md-6">
                  <a href="{{ route('app.surveys.index') }}" class="btn btn-danger pull-left">Cancelar</a>
                </div>
                <div class="col-md-6">
                  <button class="btn btn-success pull-right" ng-click="update_survey()">Guardar</button>
                </div>
              </div>
            </div>
          </form>
          <br />
        </div>
      </div>
    </div>
@stop
 