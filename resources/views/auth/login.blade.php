@extends('app.layouts.master')

@section('master-container')
@section('js')
    <!-- for checkbox -->
    <link href="{{ asset('vendor/sb-admin-2/dist/css/green.css')}}" rel="stylesheet">
        <!-- checkbox -->
    <script src="{{ asset('vendor/sb-admin-2/dist/js/icheck.min.js')}}"></script>
    <script type="text/javascript">
            /* for checkbox*/
$(document).ready(function(){
  $('input').iCheck({
    checkboxClass: 'icheckbox_flat-green',
    radioClass: 'iradio_flat-green'
  });
});
    </script>
@stop
    <style>
    body{
        background: url('{{ asset('images/fondo.jpg')}}') no-repeat fixed;
        background-size: cover;
    }
    input, button {
    outline: none;
    border: none;
}

*, *:before, *:after {
    box-sizing: border-box;
    margin: 0;
    padding: 0;
}


    </style>
    <div id="loader-wrapper">
            <div id="loader"></div>

            <div class="loader-section section-left"></div>
            <div class="loader-section section-right"></div>

    </div>
    <div class="container">
        <div class="row">
            @include('app.layouts.fragments.alerts')
            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                <img style="width: 350px;margin-top: 25%;" 
                src="{{ asset('images/logo/logo_grande.png')}}" 
                class="img img-responsive" alt="logo centrum">
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="login-panel panel panel-default">
                   <!--  <div class="panel-heading">
                        <img src="{{ asset('images/encuestas-logo-200.png')}}" class="img img-responsive center-block" alt="">
                    </div> -->
                    <div class="panel-body">
                        <form role="form" method="POST" action="{{ url('auth/login') }}">
                            {!! csrf_field() !!}
                            <fieldset>
                                <div class="login__row form-group {{ !empty(old('email')) ? 'has-error' : '' }}">
                                    <i class="fa fa-user"></i>
                                    <input class="newinput" placeholder="E-mail" name="email" type="email" value="{{ old('email') }}" autofocus>
                                </div>
                                <div class="login__row form-group">
                                    <i class="fa fa-key"></i>
                                    <input class="newinput" placeholder="Contraseña" name="password" type="password" value="">
                                </div>
                                <div class="checkbox">
                                   <!--  <label>
                                        <input  name="remember" type="checkbox" value="remember">No cerrar sesión
                                    </label> -->
                                    <input tabindex="21" type="checkbox" id="polaris-checkbox-1" checked>
                                    <label for="polaris-checkbox-1" style="padding-left: 10px;font-family: Arial;color: #929191;font-size: 1.5rem;">
                                    No cerrar sesión</label>
                                </div>
                                <!-- Change this to a button or input when using this as a form -->
                                <button type="submit" class="btn btnlogin pull-right">
                                Iniciar Sessión</button>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
