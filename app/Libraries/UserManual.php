<?php

namespace Encuestas\Libraries;

class UserManual
{
	private static $manual = [
    	[
    		'title' => 'Inicio de sesión',
    		'id'	=> 'inicio-se-sesion',
    		'image' => 1,
    		'steps' => [
    			'Escriba su email',
    			'Y su contraseña',
    			'Si desesa mantener su sesión iniciada, active la casilla',
    			'Finalmente haga click en el boton para iniciar sesión'
    		]
    	],
    	[
    		'title' => 'Mis encuestas',
    		'id'	=> 'mis-encuestas',
    		'image' => 2,
    		'steps' => [
    			'Botón de acción de usuario',
    			'Acceso al perfil del usuario',
    			'Enlace de cierre de sesión',
    			'Botón de creación de nueva encuesta',
    			'Las encuestas "Pendientes" se muestran en color amarillo',
    			'Las encuestas "Activas" se muestran en color celeste',
    			'Las encuestas "Finalizadas" se muestran en color verde',
    			'Solo se pueden editar las encuestas "pendientes"',
    			'Se pueden eliminar las encuestas "inactivas" y "pendientes"',
    			'Se pueden ver los resultados de las encuestas "activas"',
    			'Se pueden dar por finalizadas las encuestas "activas" solamente',
    			'Se podrán desactivar las encuestas "activas"',
    			'Las encuestas "Inactivas" se muestran de color blanco, se pueden activas y eliminar'
    		]
    	],
    	[
    		'title' => 'Mi cuenta',
    		'id' 	=> 'mi-cuenta',
    		'image' => 3,
    		'steps' => [
    			'Se muestra su información personal',
    			'Y su información de su actual suscripción',
    			'Si desesa cambiar su contraseña rellene este campo',
    			'y haga click en el boton guardar para completar el cambio de contraseña'
    		]
    	],
    	[
    		'title' => 'Crear nueva encuesta',
    		'id' 	=> 'crear-nueva-encuesta',
    		'image' => 4,
    		'steps' => [
    			'Escriba un título para la encuesta, por defecto siempre será "Encuesta sin título"',
    			'Elija una fecha para iniciar la encuesta',
    			'Si desea puede elegir una fecha de fin. De no elegir ninguna la encuesta finalizará cuando se alcance la cantidad de encuestados objetivo',
    			'La cantidad de encuestados es el tamaño de la muestra objetivo y esta limitada a la cantidad máxima de muestra disponible que le brinde su plan',
    			'Elija un filtro de género',
    			'Si desea activar algun filtro de edad marque la casilla correspondiente',
    			'Elija esta opción si desea una opción de edad simple',
    			'O escoja esta si desea una opcion de edad por rango',
    			'Para agregar preguntas a la encuesta utilice cualquiera de estos botones',
    			'Cuando todo este listo haga click en el botón finalizar',
    			'Caso contrario haga click en el botón cancelar para volver a la lista de encuestas'
    		]
    	],
    	[
    		'title' => 'Añadir pregunta a la encuesta',
    		'id' 	=> 'anadir-pregunta-a-la-encuesta',
    		'image' => 5,
    		'steps' => [
    			'Al hacer clicel el boton "agregar elemento" escoja "opción única" si desea una pregunta de respuesta única',
    			'Si desea una pregunta de respuesta múltiple, escoja la opción "selección múltiple'
    		]
    	],
    	[
    		'title' => 'Detalles de la pregunta',
    		'id' 	=> 'detalles-de-la-pregunta',
    		'image' => 6,
    		'steps' => [
    			'Cuando ha escogido el tipo de pregunta a crear, una sección de pregunta se añadirá a lienzo de la encuesta.',
    			'Escriba la pregunta',
    			'Si desesa puede agregar un texto de ayuda, este campo es opcional',
    			'Desmarque la casilla si la pregunta es opcional',
    			'Cuando este listo haga click en el botón "agregar" de la pregunta para agregar alternativas de respuesta',
    			'Si deses eliminar la pregunta haga click en el botón naranja correspondiente'
    		]
    	],
    	[
    		'title' => 'Detalle de alternativa',
    		'id' 	=> 'detalle-de-alternativa',
    		'image' => 7,
    		'steps' => [
    			'Al hacer click en el botón "agregar" de la pregunta una nueva alternativa se añadirá al lienzo de la pregunta.',
    			'Escriba el detalle de la alternativa de respuesta en el cuadro de texto',
    			'Si desea eliminar alguna alternativa haga click en el botón "X" situado al costado derecho de cada alternativa',
    		]
    	],
    	[
    		'title' => 'Editar una encuesta',
    		'id' 	=> 'editar-una-encuesta',
    		'image' => 8,
    		'steps' => [
    			'Puede cambiar el título de la encuesta',
    			'Corregir la fecha de inicio',
    			'Y colocar una fecha de fin si no lo hubiese hecho antes',
    			'Puede cambiar el tamaño de la muestra objetivo',
    			'Editar el filtro de género',
    			'Y el de edad si asi lo desea',
    			'Puede cambiar la condición simple por otra',
    			'O escoger un rango de edades aplicable a la encuesta',
    			'Cuando todo se halle listo guarde los cambios haciendo click en el botón celeste "guardar"',
    			'O puede cancelar la operación haciendo click en el botón rojo "cancelar" ubicado en la parte superior derecha'
    		]
    	],
    	[
    		'title' => 'Resultados de una encuesta',
    		'id' 	=> 'resultados-de-una-encuesta',
    		'image' => 9,
    		'steps' => [
    			'Al ver los resultados vera un breve resumen con los datos de la encuesta',
    			'Podra ver el dealle de la pregunta correspondiente',
    			'Y se mostrará un gráfico estádistico mostrando los resultados de dicha pregunta',
    		]
    	],
    ];

	public static function getManual() {
		return self::$manual;
	}
}