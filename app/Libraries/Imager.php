<?php

namespace Encuestas\Libraries;
use Carbon;
use Image;
use \DateTime;

class Imager {

	public static function save($source, $name = null, $path = '/images/upload') {
		try {
			$upload_images_path = public_path().'/'.$path.'/';
			
			$t = microtime(true);
			$micro = sprintf("%06d",($t - floor($t)) * 1000000);
			$d = new DateTime( date('Y-m-d H:i:s.'.$micro, $t) );
			$time = $d->format("YmdHisu");

			$image_name = is_null($name) ? $time.".png" : $name.".png";

			$filename = $upload_images_path.$image_name;
		   	$img = Image::make($source)->resize(500, 281, function ($constraint) {
				$constraint->aspectRatio();
			});
		   	$img->save($filename);
		    return $image_name;
		} catch (Exception $e) {
			return false;
		}
	}

	public static function saveAlternative($source, $name = null, $path = '/images/upload') {
		try {
			$upload_images_path = public_path().'/'.$path.'/';
			
			$t = microtime(true);
			$micro = sprintf("%06d",($t - floor($t)) * 1000000);
			$d = new DateTime( date('Y-m-d H:i:s.'.$micro, $t) );
			$time = $d->format("YmdHisu");

			$image_name = is_null($name) ? $time.".png" : $name.".png";

			$filename = $upload_images_path.$image_name;
		   	$img = Image::make($source)->resize(150, 150, function ($constraint) {
				$constraint->aspectRatio();
			});
		   	$img->save($filename);
		    return $image_name;
		} catch (Exception $e) {
			return false;
		}
	}
}