<?php

namespace Encuestas\Libraries;

use integer;

class Table
{
	public static function getRowState($state_id)
	{
		$class = '';
		switch ($state_id) {
			case 1:
				$class = 'class=warning';
				break;
			case 2:
				$class = 'class=info';
				break;
			case 3:
				$class = 'class=success';
				break;			
			default:
				$class = '';
				break;
		}
		return $class;
	}
}