<?php

namespace Encuestas\Libraries;

class ResultChart
{
	private $charts_divs = array();
	private $js = array();

	public function __construct($results) {
		foreach ($results as $key => $result){
			$this->singleJS( "chart".$key, $result['question'], json_encode($result['alternatives']) );
			$this->singleDIV( $key );
		}
	}

	private function singleDIV($question_id) {
		$chart_div_id = "chart".$question_id;
		$singleDIV = <<<EOD
			<div id="$chart_div_id"></div>
EOD;
		$this->charts_divs[$question_id] = $singleDIV;

	}

	private function singleJS($_chart_div_id, $_chart_title, $_json_data) {
		$singleJS = <<<EOD
			$(function () {
	        
	            $('#$_chart_div_id').highcharts({
	                chart: {
	                    plotBackgroundColor: null,
	                    plotBorderWidth: null,
	                    plotShadow: false,
	                    type: 'pie'
	                },
	                title: {
	                    text: '$_chart_title'
	                },
	                tooltip: {
	                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
	                },
	                plotOptions: {
	                    pie: {
	                        allowPointSelect: true,
	                        cursor: 'pointer',
	                        dataLabels: {
	                            enabled: true,
	                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
	                            style: {
	                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
	                            }
	                        }
	                    }
	                },
	                series: [{
	                    name: 'Resultado',
	                    colorByPoint: true,
	                    data: $_json_data
	                }]
	            });
	        });
EOD;
		array_push($this->js, $singleJS);
	}

	public function getCharts() {
		return implode("", $this->charts_divs);
	}

	public function getDivs() {
		return $this->charts_divs;
	}

	public function getJS() {
		return '<script type="text/javascript">' . implode("", $this->js) . '</script>';
	}
}