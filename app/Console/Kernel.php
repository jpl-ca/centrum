<?php

namespace Encuestas\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \Encuestas\Console\Commands\ActivateSurveys::class,
        \Encuestas\Console\Commands\FinishSurveys::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('survey:activate')
            ->everyFiveMinutes()->withoutOverlapping();

        $schedule->command('survey:finish')
            ->dailyAt('00:00')->withoutOverlapping();
    }
}
