<?php

namespace Encuestas\Models;

use Illuminate\Database\Eloquent\Model;

class UserSurvey extends Model
{

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'completed' => 'boolean'
    ];

    public function user()
    {
        return $this->belongsTo('Encuestas\Models\User', 'user_id', 'id');
    }

    public function survey()
    {
        return $this->belongsTo('Encuestas\Models\survey', 'survey_id', 'id');
    }
    
}
