<?php

namespace Encuestas\Models;

use Illuminate\Database\Eloquent\Model;

class AnswerAlternative extends Model
{
    protected $appends = ['totalAnswers'];
    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
    ];

    public function question()
    {
        return $this->belongsTo('Encuestas\Models\Question', 'question_id', 'id');
    }

    public function alternative_type()
    {
        return $this->belongsTo('Encuestas\Models\AlternativeType', 'alternative_type_id', 'id');
    }

    public function answers()
    {
        return $this->hasMany('Encuestas\Models\Answer', 'answer_alternative_id', 'id');
    }

    public function getTotalAnswersAttribute()
    {
        return Answer::where('answer_alternative_id', '=', $this->id)
            ->where('enabled', '=', 1)
            ->count();
    }
}
