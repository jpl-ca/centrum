<?php

namespace Encuestas\Models;

use Illuminate\Database\Eloquent\Model;

class UserGroup extends Model
{
	public function user()
	{
		return $this->belongsTo('Encuestas\Models\User','user_id', 'id');
	}

	public function group()
	{
		return $this->belongsTo('Encuestas\Models\Group', 'group_id', 'id');
	}
}
