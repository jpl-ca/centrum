<?php

namespace Encuestas\Models;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
	protected $fillable = ["name", "description"];
	protected $appends = ["totalWeight", "averageQuestionAnswers", "medianQuestionAnswers"];
	public function questions()
	{
		return $this->hasMany('Encuestas\Models\Question','section_id','id');
	}

	public function getTotalWeightAttribute()
	{
		return $this->questions->sum('weight');
	}

	public function  getAverageQuestionAnswersAttribute()
	{
		$average = 0;
		foreach ($this->questions as $question) {
			$average += $question->weight * $question->getStatsAverageAnswersAttribute();
		}
		return round($average/$this->getTotalWeightAttribute(), 2);
	}

	public function  getMedianQuestionAnswersAttribute()
	{
		$median = 0;
		foreach ($this->questions as $question) {
			$median += $question->weight * $question->getStatsMedianAnswersAttribute();
		}
		return round($median/$this->getTotalWeightAttribute(), 2);
	}
}
