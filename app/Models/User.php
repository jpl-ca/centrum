<?php

namespace Encuestas\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

use \Carbon\Carbon;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];
    
    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
    ];

    static $rules_usuario = [
        'id_doc_number' => 'required|digits:8|unique:users,id_doc_number',
        'first_name' => 'required|max:255',
        'last_name' => 'required|max:255',
        'phone' => 'numeric|digits_between:7,15',
        'birth_date' => 'required|date_format:Y-m-d',
        'gender' => 'required|boolean',
        'email' => 'required|email|max:255|unique:users,email',
        'password' => 'required|confirmed|min:6',
    ];

    public function expiration_date($format = 'Y-m-d') {
        if(is_null($this->expire_on)) return '';
        return (new \Carbon\Carbon($this->expire_on))->format($format);
    }

    public function user_type()
    {
        return $this->belongsTo('Encuestas\Models\UserType', 'user_type_id', 'id');
    }

    public function subscription_plan()
    {
        return $this->belongsTo('Encuestas\Models\SubscriptionPlan', 'subscription_plan_id', 'id');
    }

    public function subscription_history()
    {
        return $this->hasMany('Encuestas\Models\SubscriptionHistory', 'user_id', 'id');
    }

    public function current_plan()
    {
        return $this->subscription_history()->orderBy('end_date', 'desc')->first();
    }

    public function surveys()
    {
        return $this->hasMany('Encuestas\Models\Survey', 'belongs_to_user_id', 'id');
    }

    public function allowed_surveys()
    {
        return $this->hasManyThrough('Encuestas\Models\Survey','Encuestas\Models\UserSurvey', 'user_id', 'survey_id');
    }

    public function max_available_surveys()
    {
        $current_plan_info = $this->current_plan();

        //plan vitalicio sin limites
        if($current_plan_info->max_completed_per_survey == -1 || $current_plan_info->available_completed_surveys == -1) {
            return 9999999;
        }

        $max_completed_per_survey = $current_plan_info->max_completed_per_survey;

        $available_completed_surveys = $current_plan_info->available_completed_surveys;

        if( !is_null($max_completed_per_survey) ) {
            if($available_completed_surveys > $max_completed_per_survey) return $max_completed_per_survey;
        }

        return $available_completed_surveys;
    }

    public function age()
    {
        if(!is_null($this->birth_date)) {
            $bd = new Carbon($this->birth_date);
            return $bd->diffInYears(Carbon::now());
        }
        return 0;
    }

    public function groups()
    {
        return $this->belongsToMany('Encuestas\Models\Group', 'user_groups');
    }
}
