<?php

namespace Encuestas\Models;

use Illuminate\Database\Eloquent\Model;

class SurveyConstraintType extends Model
{
    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
    ];
    
}
