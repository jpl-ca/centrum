<?php

namespace Encuestas\Models;

use Illuminate\Database\Eloquent\Model;
class Group extends Model
{
	protected $fillable = ['name', 'course_name', 'program_type_name', 'program_name', 'phase_name', 'section_name'];
    public function surveys()
    {
    	$this->hasMany('Encuestas\Models\Survey', 'group_id', 'id');
    }

    public function users()
    {
    	return $this->belongsToMany('Encuestas\Models\User', 'user_groups');
    }
}
