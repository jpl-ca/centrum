<?php

namespace Encuestas;

use Illuminate\Database\Eloquent\Model;

class SurveyType extends Model
{
    public function surveys()
    {
    	return $this->hasMany('Encuestas\Models\Survey', 'survey_type_id', 'id');
    }
}
