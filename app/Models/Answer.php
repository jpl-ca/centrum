<?php

namespace Encuestas\Models;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
    ];

    public function answer_alternative()
    {
        return $this->belongsTo('Encuestas\Models\Answeralternative', 'answer_alternative_id', 'id');
    }

    public function getEnabledAttribute($status)
    {
        return (bool) $status;
    }
}
