<?php

namespace Encuestas\Models;

use Illuminate\Database\Eloquent\Model;
use Encuestas\Models\Answer;
use Encuestas\Models\AnswerAlternative;
class Question extends Model
{
    const UNIQUE_OPTION_TYPE = 1;
    const MULTIPLE_OPTION_TYPE = 2;
    const RANGE_OPTION_TYPE = 3;
    protected $appends = ['statsTotalAnswers', 'statsAverageAnswers', 'statsMedianAnswers', 'statsFirstQuartile', 'statsThirdQuartile', 'statsIc', 'statsUpperLimit', 'statsLowerLimit'];
    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
    ];

    public function survey()
    {
        return $this->belongsTo('Encuestas\Models\Survey', 'survey_id', 'id');
    }

    public function question_type()
    {
        return $this->belongsTo('Encuestas\Models\QuestionType', 'question_type_id', 'id');
    }

    public function answer_alternatives()
    {
        return $this->hasMany('Encuestas\Models\AnswerAlternative', 'question_id', 'id');
    }

    public function answers()
    {
        return $this->hasManyThrough('Encuestas\Models\Answer','Encuestas\Models\AnswerAlternative', 'question_id', 'answer_alternative_id');
    }

    public function section()
    {
        return $this->belongsTo('Encuestas\Models\Section','section_id','id');
    }

    public function anwser_alternatives()
    {
        return $this->hasMany('Encuestas\Models\AnswerAlternative', 'question_id', 'id');
    }

    public function getStatsTotalAnswersAttribute()
    {
        $total = 0;
       foreach($this->answer_alternatives as $alternative){
            $total += $alternative->getTotalAnswersAttribute();
       }
       return $total;
    }

    public function getStatsAverageAnswersAttribute()
    {
        $total = 0;
        $average = 0;
        foreach ($this->answer_alternatives as $alternative) {
            $total += $alternative->value * $alternative->getTotalAnswersAttribute();
        }
        $divider = $this->getStatsTotalAnswersAttribute();

        if($divider>0){
            $average = $total/$divider;    
        }
        
        return round($average, 2);
    }

    public function getValuesOfAnswers()
    {
        $values = [];
        foreach ($this->answers as $answer) {
            $values[] = $answer->answer_alternative->value;
        }
        return $values;
    }

    public function getStatsMedianAnswersAttribute()
    {   
        $values = $this->getValuesOfAnswers();
        return self::calculateQuartile($values, .5);
    }

    public function getStatsFirstQuartileAttribute()
    {
        $values = $this->getValuesOfAnswers();
        return self::calculateQuartile($values, .25);
    }

    public function getStatsThirdQuartileAttribute()
    {
        $values = $this->getValuesOfAnswers();
        return self::calculateQuartile($values, .75);
    }

    public function getStatsIcAttribute()
    {
        $r = 0;
        $r = $this->getStatsThirdQuartileAttribute() - $this->getStatsFirstQuartileAttribute();
        return $r;
    }

    public function getStatsUpperLimitAttribute()
    {
        $r = 0;
        $r = $this->getStatsThirdQuartileAttribute() + 1.5 * $this->getStatsIcAttribute();
        return $r;
    }

    public function getStatsLowerLimitAttribute()
    {
        $r = 0;
        $r = $this->getStatsFirstQuartileAttribute() - 1.5 * $this->getStatsIcAttribute();
        return $r;
    }

    static function calculateQuartile($Array = array(), $Quartile = .25)
    {
        $value = 0;
        $count = count($Array);
        $result = 0;
        if($count>=3)
        {
            $pos = (count($Array) - 1) * $Quartile;
            $base = floor($pos);
            $rest = $pos - $base;

            if( isset($Array[$base+1]) ) {
              $result = $Array[$base] + $rest * ($Array[$base+1] - $Array[$base]);
            } else {
              $result = $Array[$base];
            } 
        }
        return $result;
    }
}
