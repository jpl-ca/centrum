<?php

namespace Encuestas\Models;

class Survey extends EncuestaModel
{
    protected $table = "surveys";
    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    const TEMPLATE_TYPE = '2';
    const NORMAL_TYPE = '1';
    protected $casts = [
    ];

    public function __construct(array $attributes = array())
    {

        $this->setRawAttributes(
            array_merge(
                    $this->attributes,
                    [
                        'survey_type_id' => self::NORMAL_TYPE,
                    ]
                ),true);

        parent::__construct($attributes);
    }

    public function survey_state()
    {
        return $this->belongsTo('Encuestas\Models\SurveyState', 'survey_state_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo('Encuestas\Models\User', 'belongs_to_user_id', 'id');
    }

    public function allowed_users()
    {
        return $this->hasManyThrough('Encuestas\Models\User','Encuestas\Models\UserSurvey', 'survey_id', 'user_id');
    }

    public function questions()
    {
        return $this->hasMany('Encuestas\Models\Question', 'survey_id', 'id');
    }

    public function survey_constraints()
    {
        return $this->hasMany('Encuestas\Models\SurveyConstraint', 'survey_id', 'id');
    }

    public function has_constraints()
    {
        if(count($this->survey_constraints()->get()->toArray()) < 1) {
            return false;
        }
        return true;
    }

    public function gender_constraint()
    {
        return $this->survey_constraints()->where('survey_constraint_type_id', 2)->first();
    }

    public function has_gender_constraint()
    {
        if(is_null($this->gender_constraint())) {
            return false;
        }
        return true;
    }

    public function age_constraint()
    {
        return $this->survey_constraints()->where('survey_constraint_type_id', 1)->first();
    }

    public function has_age_constraint()
    {
        if(is_null($this->age_constraint())) {
            return false;
        }
        return true;
    }

    public function has_aproved_constraints(User $user)
    {        
        $approved_constraints = 0;
        $survey_constraints = $this->survey_constraints;

        foreach ($survey_constraints as $constraint) {
            if($constraint->survey_constraint_type_id == 2){
                if($constraint->value1 == $user->gender) {
                    $approved_constraints++;
                }
            }
            if($constraint->survey_constraint_type_id == 1) {
                if(is_null($constraint->value3)) {
                    switch ($constraint->value2) {
                        case '=':
                            if( $constraint->value1 == $user->age() ) {
                                $approved_constraints++;
                            }
                            break;

                        case '<':
                            if( $constraint->value1 >= $user->age() ) {
                                $approved_constraints++;
                            }
                            break;
                        case '>':
                            if( $constraint->value1 <= $user->age() ) {
                                $approved_constraints++;
                            }
                            break;
                    }
                }else {
                    if( $constraint->value1 <= $user->age() && $user->age() <= $constraint->value4 ) {
                        $approved_constraints++;
                    }
                }
            }
        }
        if($approved_constraints == count($survey_constraints)) {
            return true;
        }
        return false;
    }

    public function survey_type()
    {
        return $this->belongsTo('Encuestas\Models\SurveyType','survey_type', 'id');
    }

    public function sections()
    {
        return $this->hasMany('Encuestas\Models\Section', 'survey_id', 'id');
    }

    public function group()
    {
        return $this->belongsTo('Encuestas\Models\Group', 'group_id', 'id');
    }

    public function getStartDateAttribute()
    {
        return \Carbon::parse($this->attributes["start_date"])->format('Y-m-d');
    }

    public function getEndDateAttribute()
    {
        return \Carbon::parse($this->attributes["end_date"])->format('Y-m-d');
    }
}
