<?php

namespace Encuestas\Models;

use Illuminate\Database\Eloquent\Model;

class SurveyConstraint extends Model
{
    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
    ];

    public function survey()
    {
        return $this->belongsTo('Encuestas\Models\Survey', 'survey_id', 'id');
    }

    public function survey_constraint_type()
    {
        return $this->belongsTo('Encuestas\Models\SurveyConstraintType', 'survey_constraint_type_id', 'id');
    }

}
