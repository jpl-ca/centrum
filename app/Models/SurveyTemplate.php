<?php

namespace Encuestas\Models;

class SurveyTemplate extends Survey
{
	public function __construct(array $attributes = array())
	{

		$this->setRawAttributes(
			array_merge(
					$this->attributes,
					[
						'survey_type_id' => self::TEMPLATE_TYPE,
					]
				),true);

		parent::__construct($attributes);
	}
}