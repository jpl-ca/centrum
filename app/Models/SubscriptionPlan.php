<?php

namespace Encuestas\Models;

use Illuminate\Database\Eloquent\Model;

class SubscriptionPlan extends Model
{
    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
    ];

    public function users()
    {
        return $this->hasMany('Encuestas\Models\User', 'subscription_plan_id', 'id');
    }

}
