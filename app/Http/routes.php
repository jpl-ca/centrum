<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('image', function (Illuminate\Http\Request $request) {
	return view('image');
});

Route::post('image', function (Illuminate\Http\Request $request) {
	$msg = '';

	if($request->has('image_url') || $request->hasFile('image_file')){
		$msg .= 'Si hay imagen, ';
	}

	return $msg;

	if($request->hasFile('image_file')){
		$image = $request->file('image_file');
		\Encuestas\Libraries\Imager::save($image);
		$msg = 'imagen subida';
	}else{
		$msg = 'no hay imagen';
	}
	return $msg;
});


Route::group( [ 'namespace' => 'API', 'prefix' => 'api' ], function ()
{

	Route::controllers([
		'auth' => 'Auth\AuthController',
		'password' => 'Auth\PasswordController',
	]);

	Route::controller('survey', 'SurveyController', [
		'getIndex' => 'api.survey.index',
		'postStore' => 'api.survey.store',
		'getShow/{survey_id}' => 'api.survey.show'
	]);

	Route::controller('user', 'UserController', [
		'postRegister' => 'api.user.register',
		'postUpdateLastPosition' => 'api.user.update-last-position',
	]);

	Route::resource('groups', 'GroupController');
});

Route::group( ['prefix' => 'admin'], function(){
	Route::resource('groups', 'GroupController');
	Route::resource('survey-templates', 'SurveyTemplateController');
	Route::get('reports/survey/graphic-a/{survey_id}', 'ReportsController@graphicA');
	Route::get('reports/survey/{survey_id}', 'ReportsController@survey');
	Route::get('surveys/{survey_id}', 'SurveyController@getVer');
	Route::get('surveys/{survey_id}/editar', 'SurveyController@getEditarEncuesta');
	Route::put('surveys/{survey_id}', 'SurveyController@putActualizar');
	Route::delete('surveys/{survey_id}', 'SurveyController@deleteEliminarEncuesta');
	Route::get('surveys/evaluate/{survey_id}', 'SurveyController@getVerEvaluacionEncuesta');
	Route::post('surveys/evaluate', 'SurveyController@postEvaluacionEncuesta');
	Route::post('surveys/copy-template', 'SurveyController@postCopiaGuardaPlantilla');
	Route::get('surveys', 'SurveyController@getIndex');
});

// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');

Route::controller('mis-encuestas', 'SurveyController', [
    'getIndex' => 'app.surveys.index',
    'getCrear' => 'app.surveys.create',
    'postGuardar' => 'app.surveys.store',
    'getVerResultados' => 'app.surveys.results',
    'getFinalizar' => 'app.surveys.finish',
    'getActivar' => 'app.surveys.activate',
    'getDesactivar' => 'app.surveys.deactivate',
    'getEliminar' => 'app.surveys.delete',
    'getEditar' => 'app.surveys.edit',
    'postActualizar' => 'app.surveys.update',
    'postCopiaGuardaPlantillla' => 'app.surveys.copyTemplate',
    'getCrearTemplate' => 'app.templates.create',
    'getAprobarRespuestas' => 'app.surveys.aprobar'
]);



Route::get('dashboard', ['as' => 'dashboard', 'uses' => 'AppController@getDashboard']);

Route::get('manual-de-usuario', function(Illuminate\Http\Request $request){

    $pagename = 'Manual de usuario';
    $pagetitle = 'Manual de usuario';
    $subtitle = '';

    $manual = Encuestas\Libraries\UserManual::getManual();

	return view('app.user-manual.index', compact('pagename','pagetitle','subtitle','manual'));
});

Route::controller('/', 'UserController', [
    'getMiCuenta' => 'app.users.profile',
    'postMiCuenta' => 'app.users.update-profile',
    'getConfiguracion' => 'app.users.configuration',
    'postConfiguracion' => 'app.users.update-configuration',
]);