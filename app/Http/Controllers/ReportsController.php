<?php

namespace Encuestas\Http\Controllers;

use Illuminate\Http\Request;

use Encuestas\Http\Requests;
use Encuestas\Http\Controllers\Controller;
use Encuestas\Models\Survey;
use Encuestas\Libraries\Response;
use Maatwebsite\Excel\Facades\Excel;
class ReportsController extends Controller
{
    public function survey(Request $request, $survey_id)
    {
        $survey = Survey::findOrFail($survey_id);
        $survey->sections;
        foreach ($survey->sections as $section) {
            //$section->questions->load('answers.alternatives');
            foreach ($section->questions as $question) {
                foreach($question->answer_alternatives as $answer_alternative){

                }
            }
        }
        if($request->has("format")){
            if($request->format=="json"){
                return Response::ok($survey->toArray());
            }
        }else{

        $this->pagename = 'Consolidado  de Encuestas';
        $this->pagetitle = '';
        $this->subtitle = '';
            return $this->view('app.reports.view', compact('survey'));
        }
        
    }

    public function graphicA(Request $request, $survey_id)
    {
        $survey = Survey::findOrFail($survey_id);

        $series = [
            "Promedio de conceptos",
            "Límite de especificaciones",
            "Promedio de profesor",
            "Promedio de curso"
        ];

        $labels = [];
        $short_labels = [];
        $averageOfQuestions = [];
        foreach ($survey->sections as $section) {
            foreach ($section->questions as $question) {
                $labels[] = $question->title;
                $short_labels[] = str_limit($question->title, $limit = 10, $end = '...');
                $averageOfQuestions[] = $question->getStatsAverageAnswersAttribute();
            }
        }
        $a = [];
        $b = [];
        $c = [];
        $averageOfTeacherSection = 0;
        $averageOfCourseSection = 0;
        if(isset($survey->sections[0]) && isset($survey->sections[1])){
            $averageOfTeacherSection = $survey->sections[0]->averageQuestionAnswers;
            $averageOfCourseSection = $survey->sections[1]->averageQuestionAnswers;
        }
        for($i = 0; $i < count($labels); $i++) {
            $a[] = 4;
            $b[] = $averageOfTeacherSection;
            $c[] = $averageOfCourseSection;
        }
        if($request->has("format")){
            if($request->format=="json"){
                $data["title"] = $survey->title;
                $data["series"] = $series;
                $data["labels"] = $labels;
                $data["short_labels"] = $short_labels;
                $data["data"] = [
                $averageOfQuestions,
                $a,
                $b,
                $c
                ];
                return Response::ok($data);
            }

            if($request->format == "xls"){        
                Excel::create('Reporte', function($excel) use ($survey, $a, $b, $c, $averageOfQuestions){
                    $excel->sheet('reporte', function($sheet) use ($survey, $a, $b, $c, $averageOfQuestions) {

                       $sheet->setWidth('A', 60);
                       $sheet->setWidth('B', 10);
                       $i = 0;

                       $i++;
                       $sheet->row($i, array(
                            "Programa",
                             $survey->group->program_name
                        ));

                       $sheet->row($i, function($row) {
                            $row->setBackground('#BAE4E5');
                        });

                       $i++;
                       $sheet->row($i, array(
                            "Ciclo",
                             $survey->group->phase_name
                        ));

                       $sheet->row($i, function($row) {
                            $row->setBackground('#BAE4E5');
                        });

                       $i++;
                       $sheet->row($i, array(
                            "Profesor",
                             $survey->group->teacher_name
                        ));
                       $sheet->row($i, function($row) {
                            $row->setBackground('#BAE4E5');
                        });

                       $i++;
                       $sheet->row($i, array(
                            "Tutor",
                             $survey->group->tutor_name
                        ));
                       $sheet->row($i, function($row) {
                            $row->setBackground('#BAE4E5');
                        });

                        $i++;
                        $sheet->row($i, array(
                            "Curso",
                             $survey->group->name
                        ));
                        $sheet->row($i, function($row) {
                            $row->setBackground('#BAE4E5');
                        });

                        $i++;
                        $sheet->row($i, array(
                            "Sesiones"
                        ));
                        $sheet->row($i, function($row) {
                            $row->setBackground('#BAE4E5');
                        });

                        $i++;
                        $sheet->row($i, array(
                            "Alumnos Encuestados",
                             $survey->current_completed_surveys
                        ));
                        $sheet->row($i, function($row) {
                            $row->setBackground('#BAE4E5');
                        });

                        $i++;
                        $sheet->row($i, array(
                            "Alumnos inscritos /presentes"
                        ));
                        $sheet->row($i, function($row) {
                            $row->setBackground('#BAE4E5');
                        });

                        $i++;
                        $sheet->row($i, array(
                            "Encuestas"
                        ));
                        $sheet->row($i, function($row) {
                            $row->setBackground('#BAE4E5');
                        });

                        $header_a = ["Encuestas", 'Cuentas', '', '', '', ''];
                        $header_b = ['Tot', "Peso", "Prom", "Mediana", "Quartil 1", "Quartil 3", "Alcance IC", "Lim. Inf.", "Lim. Sup.", "Respuestas"];
                        //foreach ($ ) {
                        //}
                        $header_c = [];
                        $header = array_merge($header_a, $header_b, $header_c);

                        $i++;
                        $sheet->row($i, $header);
                        $sheet->row($i, function($row) {
                                $row->setFontWeight('bold');
                        });

                        $i_section = 0;
                        foreach($survey->sections as $section){
                            $i_section++;

                            $i++;
                            if($i_section==1){
                                $row = [$section->name, '1', '2', '3', '4', '5'];
                            }else{
                                $row = [$section->name];
                            }

                            $sheet->row($i, $row);
                            $sheet->row($i, function($row) {
                                $row->setFontWeight('bold');
                            });
                            

                            foreach ($section->questions as $question) {
                                $row = [];
                                $row[] = $question->title;
                                foreach($question->answer_alternatives as $answer_alternative){
                                    $row[] = $answer_alternative->getTotalAnswersAttribute();
                                }
                                $row[] = $question->getStatsTotalAnswersAttribute();
                                $row[] = $question->weight;
                                $row[] = $question->getStatsAverageAnswersAttribute();
                                $row[] = $question->getStatsMedianAnswersAttribute();

                                // stats
                                $row[] = $question->getStatsFirstQuartileAttribute();
                                $row[] = $question->getStatsThirdQuartileAttribute();
                                $row[] = $question->getStatsIcAttribute();
                                $row[] = $question->getStatsLowerLimitAttribute();
                                $row[] = $question->getStatsUpperLimitAttribute();

                                //answers
                                foreach ($question->answers as $answer) {
                                    $row[] = $answer->answer_alternative->value;
                                }

                                $i++;
                                $sheet->row($i, $row);
                            }
                            
                        }
                        $i=$i+4;
                        $sheet->row($i, ["Comentarios"]);
                        $sheet->row($i, function($row) {
                                $row->setFontWeight('bold');
                            });

                        $sheet->cells('A1:AI9', function($cells) {

                            $cells->setBackground('#BAE4E5');

                        });

                    });

                    $excel->sheet('reporte_2', function($sheet) use ($survey, $a, $b, $c, $averageOfQuestions) {

                        
                            $i=0;
                            
                            $i++;

                            $averageOfQuestions = array_merge(["Promedio de conceptos"],$averageOfQuestions);
                            $a = array_merge(["Límite de especificaciones"],$a);
                            $b = array_merge(["Promedio profesor"],$b);
                            $c =array_merge(["Promedio curso"],$c);
    
                            $sheet->row($i, $averageOfQuestions);
                            
                            $i++;
                            $sheet->row($i, $a);
                            
                            $i++;
                            $sheet->row($i, $b);
                            
                            $i++;
                            $sheet->row($i, $c);    
                        
                        
                    });
                })->download();
            }
        }
    }
}
