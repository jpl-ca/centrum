<?php

namespace Encuestas\Http\Controllers\Auth;

use Illuminate\Http\Request;

use Encuestas\Models\User;
use Validator;
use Auth;
use Encuestas\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

use PaulVL\Helpers\StringHelper;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['only' => ['getLogout']]);
        $this->middleware('guest', ['except' => ['getLogout']]);
        /*Uncomment on production

        */
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }


    public function getLogin()
    {
        $this->pagename = 'Login';

        return $this->view('auth.login');
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postLogin(Request $request)
    {
        $inputs = $request->all();

        $validator = Validator::make($inputs, [
            'email' => 'required',
            'password' => 'required',
        ]);
        
        if ($validator->fails()) {
            return redirect()->back()->withValidationErrors( $validator->errors()->all() );
        }

        if ( Auth::attempt( ['email' => $inputs['email'], 'password' => $inputs['password'], 'user_type_id' => 1], $request->has('remember') ) || Auth::attempt( ['email' => $inputs['email'], 'password' => $inputs['password'], 'user_type_id' => 2], $request->has('remember') )) {
            // Authentication passed...
            return redirect()->intended('dashboard');
        }

        return redirect()->back()->withError( $this->getFailedLoginMessage() )->withInput( $inputs );
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function getCredentials(Request $request)
    {
        $credentials = ['user_type_id' => 2]; // solo pueden iniciar sesion los usuarios
        $credentials = array_merge($credentials, $request->only($this->loginUsername(), 'password'));
        return $credentials;
    }

    /**
     * Get the failed login message.
     *
     * @return string
     */
    protected function getFailedLoginMessage()
    {
        return trans('auth.failed');
    }

    /**
     * Log the user out of the application.
     *
     * @return \Illuminate\Http\Response
     */
    public function getLogout(Request $request)
    {        
        if($request->user()) {            
            Auth::logout();
            return redirect('auth/login');        
        }
    }

    /**
     * Log the user out of the application.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCheck(Request $request)
    {
        if($request->user()) {            
            return Response::ok($request->user()->toArray());
        }
        return Response::unauthorized('No existe una sesión de usuario');
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function loginUsername()
    {
        return property_exists($this, 'username') ? $this->username : 'email';
    }

    public function postRequestPassword(){
        return Response::ok(['messsage' => "Su solicitud de re-establecimiento de contraseña está siendo procesada"]);
    }
}
