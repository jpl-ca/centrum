<?php

namespace Encuestas\Http\Controllers;

use Illuminate\Http\Request;

use Encuestas\Libraries\Response;
use Encuestas\Http\Controllers\Controller;
use Encuestas\Models\SurveyTemplate;
use Encuestas\Models\Survey;
use Encuestas\Models\Section;
use Encuestas\Models\Question;
use Encuestas\Models\AnswerAlternative;

class SurveyTemplateController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return Response::ok( SurveyTemplate::where("survey_type_id", "=", Survey::TEMPLATE_TYPE)->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $new_template = new SurveyTemplate();
        $new_template->title = $request->title;
        $new_template->description = isset($request["description"]) ? $request["description"] : '';
        $new_template->start_date = $request->start_date;
        $new_template->end_date = $request->end_date;
        $new_template->end_date = $new_template->end_date ? $new_template->end_date :  $request->start_date;
        $new_template->group_id = $request->group_id;
        $new_template->survey_state_id = $request->survey_state_id;
        $new_template->survey_type_id = Survey::TEMPLATE_TYPE;
        $new_template->img_url = $request->has('img_url') ? $request->img_url : null;
        $new_template->save();
        if($request->has('sections')){
            foreach ($request->sections as $section) {
                $new_section = new Section($section);
                $new_section->survey_id = $new_template->id;
                $new_section->save();
                if( $new_section->exists() && isset($section["questions"]) ){
                    foreach($section["questions"] as $question){
                        $new_question = new Question;
                        $new_question->survey_id = $new_template->id;
                        $new_question->section_id = $new_section->id;
                        $new_question->title = $question["title"];
                        $new_question->help_text = isset($question["help_text"]) ? $question["help_text"] : null ;
                        $new_question->question_type_id = isset($question["question_type_id"]) ? $question["question_type_id"] : Question::UNIQUE_OPTION_TYPE;
                        $new_question->mandatory = isset($question["mandatory"]) ? $question["mandatory"] : true;
                        $new_question->weight = isset($question["weight"]) ? $question["weight"] : 1;
                        $new_question->save();

                        if( $new_question->exists() && isset($question["alternatives"]) ){

                            foreach ($question["alternatives"] as $alternative) {
                                $new_alternative = new AnswerAlternative;
                                $new_alternative->alternative_type_id = $alternative['alternative_type_id'];
                                $new_alternative->description = $alternative['description'];

                                if(!isset($alternative['value'])){
                                    $new_alternative->value = (int) $alternative['description'];
                                }else{
                                    $new_alternative->value = $alternative['value'];
                                }

                                $new_alternative->img_url = isset($alternative['img_url']) ? $alternative['img_url'] : null;
                                $new_question->answer_alternatives()->save($new_alternative);
                            }

                        }
                    }
                }

            }
        }
        return Response::ok($new_template);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $new_template = SurveyTemplate::findOrFail($id);
        $new_template->sections()->delete();

        $new_template->title = $request->title;
        $new_template->description = isset($request["description"]) ? $request["description"] : '';
        $new_template->start_date = $request->start_date;
        $new_template->end_date = $request->end_date;
        $new_template->end_date = $new_template->end_date ? $new_template->end_date :  $request->start_date;
        $new_template->group_id = $request->group_id;
        $new_template->survey_state_id = $request->survey_state_id;
        $new_template->survey_type_id = Survey::TEMPLATE_TYPE;
        $new_template->img_url = $request->has('img_url') ? $request->img_url : null;
        $new_template->save();
        if($request->has('sections')){
            foreach ($request->sections as $section) {
                $new_section = new Section($section);
                $new_section->survey_id = $new_template->id;
                $new_section->save();
                if( $new_section->exists() && isset($section["questions"]) ){
                    foreach($section["questions"] as $question){
                        $new_question = new Question;
                        $new_question->survey_id = $new_template->id;
                        $new_question->section_id = $new_section->id;
                        $new_question->title = $question["title"];
                        $new_question->help_text = isset($question["help_text"]) ? $question["help_text"] : null ;
                        $new_question->question_type_id = isset($question["question_type_id"]) ? $question["question_type_id"] : Question::UNIQUE_OPTION_TYPE;
                        $new_question->mandatory = isset($question["mandatory"]) ? $question["mandatory"] : true;
                        $new_question->weight = isset($question["weight"]) ? $question["weight"] : 1;
                        $new_question->save();

                        if( $new_question->exists() && isset($question["answer_alternatives"]) && count($question["answer_alternatives"])>0){

                            foreach ($question["answer_alternatives"] as $alternative) {
                                $new_alternative = new AnswerAlternative;
                                $new_alternative->alternative_type_id = $alternative['alternative_type_id'];
                                $new_alternative->description = $alternative['description'];

                                if(!isset($alternative['value'])){
                                    $new_alternative->value = (int) $alternative['description'];
                                }else{
                                    $new_alternative->value = $alternative['value'];
                                }

                                $new_alternative->img_url = isset($alternative['img_url']) ? $alternative['img_url'] : null;
                                $new_question->answer_alternatives()->save($new_alternative);
                            }

                        }else
                        {

                            $value = 1;
                            foreach (["Nunca", "Pocas veces", "Algunas veces", "Casi siempre", "Siempre"] as $text) {
                                $new_alternative = new AnswerAlternative;
                                $new_alternative->alternative_type_id = 1;
                                $new_alternative->description = $text;
                                $new_alternative->value = $value;
                                $new_question->answer_alternatives()->save($new_alternative);
                                $value++;
                            }
                        }
                    }
                }

            }
        }
        return Response::ok($new_template);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $survey = SurveyTemplate::find($id);
        $survey->delete();
        return Response::ok(["message" => "Se eliminado esta plantilla exitosamente."]);
    }
}
