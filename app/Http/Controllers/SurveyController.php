<?php

namespace Encuestas\Http\Controllers;

use Illuminate\Http\Request;

use Encuestas\Http\Requests;
use Encuestas\Http\Controllers\Controller;

use Encuestas\Models\Survey;
use Encuestas\Models\SurveyConstraint;
use Encuestas\Models\Question;
use Encuestas\Models\AnswerAlternative;
use Encuestas\Models\SurveyTemplate;
use Encuestas\Models\Section;
use Encuestas\Libraries\Imager;
use Encuestas\Libraries\Response;
use Encuestas\Models\Answer;
use PaulVL\Helpers\StringHelper;
use Illuminate\Support\Facades\Redirect;
use Carbon\Carbon;
use Encuestas\Models\Group;
class SurveyController extends Controller
{

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('auth');
        /*Uncomment on production

        */
    }

    public function getIndex(Request $request)
    {
        $this->pagename = 'Mis Encuestas';
        $this->pagetitle = 'Mis Encuestas';
        $this->subtitle = '';

        $surveys = SurveyTemplate::where("survey_type_id", "=", Survey::NORMAL_TYPE);

        if($request->has("fields")){
            $fields = explode(',', $request->fields);
        }else{
            $fields = [];
        }

        if(count($fields) > 0){
            if(in_array('group', $fields)){
                $surveys->with('group');
            }
        }

        if($request->format == "json"){
            return Response::ok($surveys->get());
        }
        else{
            return $this->view('encuestas.index', compact('surveys'));    
        }
    }

    public function getCrear()
    {
        $this->pagename = 'Mis Encuestas';
        $this->pagetitle = 'Mis Encuestas';
        $this->subtitle = 'crear nueva encuesta';

        return $this->view('app.surveys.create', compact('max_available_surveys'));
    }

    public function postGuardar(Request $request)
    {
    	
    }

    public function getVerResultados($id)
    {
        if(auth()->user()->user_type_id != 1) {
            $survey = Survey::where('id',$id)->where('belongs_to_user_id', auth()->user()->id)->get();
        }else {
            $survey = Survey::where('id',$id)->get();
        }

        if($survey->isEmpty()) {
            return redirect()->route('app.surveys.index');
        }

        $survey = $survey->first();

        if($survey->belongs_to_user_id != auth()->user()->id) return redirect()->back();

        $this->pagename = 'Mis Encuestas';
        $this->pagetitle = 'Mis Encuestas';
        $this->subtitle = 'resultados de la encuesta #'.$survey->id;

        $results = array();

        foreach ($survey->questions as $question) {
            $results[$question->id] = ['question' => $question->title];
            $results[$question->id]['type'] = $question->question_type_id;
            $alternatives_answers = array();
            $total_answers = 0;
            foreach ($question->answer_alternatives as $answer_alternative) {
                array_push($alternatives_answers, [
                    'name' => $answer_alternative->description,
                    'y' => count($answer_alternative->answers)
                ]);
                $total_answers += count($answer_alternative->answers);
            }
            $results[$question->id]['alternatives'] = $alternatives_answers;
            $results[$question->id]['total_answers'] = $total_answers;
        }

        $chart = new \Encuestas\Libraries\ResultChart($results);

        return $this->view('app.surveys.results', compact('survey', 'results', 'chart'));
    }

    public function getFinalizar($id) {

        if(auth()->user()->user_type_id != 1) {
            $survey = Survey::where('id',$id)->where('belongs_to_user_id', auth()->user()->id)->get();
        }else {
            $survey = Survey::where('id',$id)->get();
        }

        if($survey->isEmpty()) {
            return redirect()->route('app.surveys.index');
        }

        $survey = $survey->first();

        if($survey->survey_state_id == 2) {
            $survey->survey_state_id = 3;
            $survey->save();
            return redirect()->route('app.surveys.index')->withSuccess('La encuesta ha sido finalizada correctamente.');
        }else {
            return redirect()->route('app.surveys.index')->withWarning('La encuesta tiene que estar "Activa" para poder ser finalizada.');
        }
    }

    public function getDesactivar($id) {

        if(auth()->user()->user_type_id != 1) {
            $survey = Survey::where('id',$id)->where('belongs_to_user_id', auth()->user()->id)->get();
        }else {
            $survey = Survey::where('id',$id)->get();
        }

        if($survey->isEmpty()) {
            return redirect()->route('app.surveys.index');
        }

        $survey = $survey->first();

        if($survey->survey_state_id == 2) {
            $survey->survey_state_id = 4;
            $survey->save();
            return redirect()->route('app.surveys.index')->withSuccess('La encuesta ha sido desactivada correctamente.');
        }else {
            return redirect()->route('app.surveys.index')->withWarning('La encuesta tiene que estar "Activa" para poder ser desactivada.');
        }
    }

    public function getActivar($id) {

        if(auth()->user()->user_type_id != 1) {
            $survey = Survey::where('id',$id)->where('belongs_to_user_id', auth()->user()->id)->get();
        }else {
            $survey = Survey::where('id',$id)->get();
        }

        if($survey->isEmpty()) {
            return redirect()->route('app.surveys.index');
        }

        $survey = $survey->first();

        if($survey->survey_state_id == 4) {
            $survey->survey_state_id = 2;
            $survey->save();
            return redirect()->route('app.surveys.index')->withSuccess('La encuesta ha sido activada correctamente.');
        }else {
            return redirect()->route('app.surveys.index')->withWarning('La encuesta tiene que estar "Inactiva" para poder ser activada.');
        }
    }

    public function getEliminar($id) {

        if(auth()->user()->user_type_id != 1) {
            $survey = Survey::where('id',$id)->where('belongs_to_user_id', auth()->user()->id)->get();
        }else {
            $survey = Survey::where('id',$id)->get();
        }

        if($survey->isEmpty()) {
            return redirect()->route('app.surveys.index');
        }

        $survey = $survey->first();

        if($survey->current_completed_surveys == 0) {
            $survey->delete();
            return redirect()->route('app.surveys.index')->withSuccess('La encuesta ha sido eliminada correctamente.');
        }else {
            return redirect()->route('app.surveys.index')->withWarning('La encuesta no tiene que haber sido respondida para poder ser eliminada.');
        }
    }

    public function getEditar($id) {

        if(auth()->user()->user_type_id != 1) {
            $survey = Survey::where('id',$id)->where('belongs_to_user_id', auth()->user()->id)->get();
        }else {
            $survey = Survey::where('id',$id)->get();
        }

        if($survey->isEmpty()) {
            return redirect()->route('app.surveys.index');
        }

        $survey = $survey->first();


        $this->pagename = 'Mis Encuestas';
        $this->pagetitle = 'Mis Encuestas';
        $this->subtitle = 'editando la encuesta #'.$survey->id;


        // $max_available_surveys = auth()->user()->max_available_surveys();

        return $this->view('app.surveys.edit', compact('survey'));
    }
    
    public function getEditarEncuesta(Request $request, $id) {
        $survey = Survey::find($id);
        $this->pagename = 'Mis Encuestas';
        $this->pagetitle = 'Mis Encuestas';
        $this->subtitle = 'editando la encuesta #'.$survey->id;
        return $this->view('app.surveys.editarEncuesta', compact('survey'));
    }

    public function putActualizar(Request $request, $id) {
        $survey = Survey::findOrFail($id);
        if($request->has("start_date")){
            $survey->start_date = $request->start_date;
            $survey->end_date = $survey->start_date;    
        }

        $survey->save();
        return Response::ok($survey);
    }

    public function postCopiaGuardaPlantilla(Request $request)
    {
        $survey_template = SurveyTemplate::find($request->survey_template_id);
        if(!$survey_template || !$survey_template->exists()){
            return "error";
        }

        $new_survey = new Survey();
        $new_survey->title = $survey_template->title;
        $new_survey->start_date = $request->start_date;
        $new_survey->end_date = $request->end_date;
        $new_survey->group_id = $request->group_id;
        $new_survey->survey_state_id = $request->survey_state_id;
        $new_survey->img_url = $request->has('img_url') ? $request->img_url : null;

        if($request->has("group_id")){
            $group = Group::find($request->group_id);
            $totalOfUsers = $group->users()->count();
            $new_survey->max_completed_per_survey = $totalOfUsers;
        }

        $new_survey->save();
        if($new_survey->exists() && $survey_template->sections){
            foreach ($survey_template->sections as $section) {
                $new_section = new Section;
                $new_section->name = $section->name;
                $new_section->description = $section->description;
                $new_section->survey_id = $new_survey->id;
                $new_section->save();
                if( $new_section->exists() && $section->questions) {
                    foreach($section->questions as $question){
                        $new_question = new Question;
                        $new_question->survey_id = $new_survey->id;
                        $new_question->section_id = $new_section->id;
                        $new_question->title = $question->title;
                        $new_question->help_text = $question->help_text;
                        $new_question->question_type_id = $question->question_type_id;
                        $new_question->mandatory = $question->mandatory;
                        $new_question->weight = $question->weight;
                        $new_question->save();

                        if( $new_question->exists() && $question->anwser_alternatives) {

                            foreach ($question->anwser_alternatives as $alternative) {
                                $new_alternative = new AnswerAlternative;
                                $new_alternative->alternative_type_id = $alternative->alternative_type_id;
                                $new_alternative->description = $alternative->description;
                                $new_alternative->value = $alternative->value;
                                $new_alternative->img_url = $alternative->img_url;
                                $new_question->answer_alternatives()->save($new_alternative);
                            }

                        }
                    }
                }

            }
        }

        return Response::ok($new_survey);
    }

    public function getVer(Request $request, $id){
        $survey = Survey::with('sections')->with('questions.answer_alternatives')->find($id);        

        if($request->has('fields')){
            $fields = explode(',', $request->fields);

            if(in_array('group', $fields)){
                $survey->group;
            }
        }

        foreach($survey->sections as $section){
            foreach ($section->questions as $question) {
                foreach ($question->answer_alternatives as $alternative) {
                    foreach ($alternative->answers as $answer) {
                        $answer->comment;
                    }
                }
            }
        }
        return Response::ok($survey);
    }

    public function getVerEvaluacionEncuesta(Request $request, $id)
    {
        $survey = Survey::find($id);

        foreach ($survey->questions as $question) {
            $question->load(['answers' => function($q){
                $q->where('answer_alternatives.value', '<=', 3);
            }]);
        }
        
        return Response::ok($survey);
    }

    public function postEvaluacionEncuesta(Request $request)
    {
        $survey = Survey::find($request->id);
        if(!isset($request["questions"])){
            return Response::unprocessableEntity(['message' => "No hay preguntas a evaluar"]);
        }

        foreach($request["questions"] as $question){
            if(isset($question["answers"]) and count($question["answers"])>0){
                foreach ($question["answers"] as $answer) {
                    $a = Answer::find($answer["id"]);

                    if(isset($answer["enabled"])){
                        $a->enabled = $answer["enabled"] ;    
                    }
                    $a->save();
                }
            }
        }
        return Redirect::to('admin/surveys/evaluate/'.$survey->id);
    }

    public function getCrearTemplate()
    {   
        $this->pagename = 'Creación de  Plantillas';
        $this->pagetitle = 'Crear Plantilla';
        $this->subtitle = '';
        return $this->view('app.templates.create');
    }

    public function getAprobarRespuestas($id)
    {   
        if(auth()->user()->user_type_id != 1) {
            $survey = Survey::where('id',$id)->where('belongs_to_user_id', auth()->user()->id)->get();
        }else {
            $survey = Survey::where('id',$id)->get();
        }

        if($survey->isEmpty()) {
            return redirect()->route('app.surveys.index');
        }

        $survey = $survey->first();
        $this->pagename = 'Aprobar Respuestas';
        $this->pagetitle = 'Aprobar Respuestas';
        $this->subtitle = '';
        return $this->view('app.surveys.aprobar', compact('survey'));
    }

    public function deleteEliminarEncuesta(Request $request, $id)
    {
        $survey = Survey::find($id);
        $survey->delete();
        return Response::ok(["message" => "Se eliminado esta encuesta exitosamente."]);
    }
}
