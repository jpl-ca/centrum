<?php

namespace Encuestas\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Encuestas\Libraries\Response;

class ApiAuthenticate
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($this->auth->guest()) {
            return Response::unauthorized('No existe una sesión de usuario');
        }elseif ($this->auth->user()->user_type_id != 3) {
            return Response::forbidden('No tiene permisos de acceso');
        }

        return $next($request);
    }
}
