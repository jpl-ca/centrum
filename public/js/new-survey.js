$(document).ready(function() {

    enableAgeConstraints(true);
	total_questions = 0;
	total_alternatives = 0;

    var max_fields      = 9999; //maximum input boxes allowed
    //var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var questions_wrapper         = $(".questions_wrapper"); //Fields wrapper
    //var add_button      = $(".add_alternative_button"); //Add button ID
    
    var x = 1; //initlal text box count
    var question_ider = 0; //initlal text box count

    var max_completed_per_survey = $("input[name='max_completed_per_survey']");
    var max_completed_surveys = max_completed_per_survey.attr('max');
    
    var _URL = window.URL || window.webkitURL;

    $(document).on('click', '.add_alternative_button', function(e) { //user click on remove text
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            total_alternatives++; //text box increment
            wrapper = $(this).closest('div[class^="alternatives_wrapper_"]');
            question_wrapper = $(this).closest('div[class^="question_"]');
            question_id = question_wrapper.attr('id');
            question_type = question_wrapper.attr('question-type');
            alternative_id = new Date().getTime();
            if(question_type == 'simple') {
            	input_str = get_simple_alternative(alternative_id, question_id);
            }
            if(question_type == 'multiple') {
            	input_str = get_multiple_alternative(alternative_id, question_id);
            }
            $(wrapper).append(input_str); //add input box
        }
    });

    $(".add_simple_question_button").click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed

            wrapper = $(".questions_wrapper");

            tmstmps_id = new Date().getTime();
            question_str = get_simple_question( tmstmps_id );
            $(wrapper).append(question_str); //add input box
            question_ider++;
            total_questions++;
        }
    });

    $(".add_multiple_question_button").click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed

            wrapper = $(".questions_wrapper");

            tmstmps_id = new Date().getTime();
            question_str = get_multiple_choise_question( tmstmps_id );
            $(wrapper).append(question_str); //add input box
            question_ider++;
            total_questions++;
        }
    });

    $(document).on('change', '.xfiles', function(e) {
        var file, img;
        if ((file = this.files[0])) {
            img = new Image();

            img.onload = function () {

                height = this.height;
                width = this.width;

                if( (height != 281) || (width != 500) ) {
                    showBadBannerImage();
                }
            };                
            img.src = _URL.createObjectURL(file);
        }
    });

    $(document).on('change', '.x2files', function(e) {
        var file, img;
        if ((file = this.files[0])) {
            img = new Image();
            var element = this;

            img.onload = function (){

                console.log(element);

                height = this.height;
                width = this.width;

                if( (height != 150) || (width != 150) ) {
                    showBadAlternativeImage(element);
                }
            };

            img.src = _URL.createObjectURL(file);
        }
    });

    function showBadBannerImage() {
        $.smkAlert({text:'La imagen debe de tener un tamaño de 500x281 px.', type:'danger'});
        $("#image_file").wrap('<form>').closest('form').get(0).reset();
        $("#image_file").unwrap();
    }

    function showBadAlternativeImage(element) {
        $.smkAlert({text:'La imagen debe de tener un tamaño de 150x150 px.', type:'danger'});
        $(element).wrap('<form>').closest('form').get(0).reset();
        $(element).unwrap();
    }

    $(document).on('click', '.remove_alternative', function(e) { //user click on remove text
        e.preventDefault();
        alternative = $(this).closest('div[class^="alternative_"]');
        alternative.remove();
        x--;
        total_alternatives--;
    });

    $(document).on('click', '.remove_question', function(e) { //user click on remove text
        e.preventDefault();
        alternative = $(this).closest('div[class^="question_"]');
        alternative.remove();
        question_ider--;
        total_questions--;
    });

    function get_simple_question(tmstmps_id) {
    	return '<div class="question_'+tmstmps_id+'" question-type="simple" id="'+question_ider+'"><li class="list-group-item"><div class="row"><div class="col-md-12"><button type="button" class="btn btn-warning btn-sm margin-bottom-10 remove_question">Eliminar pregunta <span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button><h5>Pregunta de selección única<h5><div class="form-group"><input class="form-control" type="text" name="question[]" value="" placeholder="Título de la pregunta" autocomplete="off" required></div><div class="form-group form-group-sm"><input class="form-control" type="text" name="helptext['+question_ider+']" placeholder="texto de ayuda" autocomplete="off"><input type="hidden" name="question_type['+question_ider+']" value="1"><div class="checkbox"><label><input type="checkbox" value="1" name="mandatory['+question_ider+']" autocomplete="off" checked>Esta pregunta es obligatoria</label></div></div><div class="alternatives_wrapper_'+tmstmps_id+'"><h5>Alternativas <a class="btn btn-default btn-xs add_alternative_button">Agregar</a></h5></div></div></div></li></div>'

    }

    function get_multiple_choise_question(tmstmps_id) {
    	return '<div class="question_'+tmstmps_id+'" question-type="multiple" id="'+question_ider+'"><li class="list-group-item"><div class="row"><div class="col-md-12"><button type="button" class="btn btn-warning btn-sm margin-bottom-10 remove_question">Eliminar pregunta <span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button><h5>Pregunta de selección múltiple<h5><div class="form-group"><input class="form-control" type="text" name="question[]" value="" placeholder="Título de la pregunta" autocomplete="off" required></div><div class="form-group form-group-sm"><input class="form-control" type="text" name="helptext['+question_ider+']" placeholder="texto de ayuda" autocomplete="off"><input type="hidden" name="question_type['+question_ider+']" value="2"><div class="checkbox"><label><input type="checkbox" value="1" name="mandatory['+question_ider+']" autocomplete="off" checked>Esta pregunta es obligatoria</label></div></div><div class="alternatives_wrapper_'+tmstmps_id+'"><h5>Alternativas <a class="btn btn-default btn-xs add_alternative_button">Agregar</a></h5></div></div></div></li></div>'

    }

    function get_simple_alternative(alternative_id, question_id) {
    	return '<div class="alternative_'+alternative_id+' form-group"><div class="input-group"><span class="input-group-addon"><input type="radio" aria-label="..." disabled></span><input type="text" class="form-control" name="alternative_description['+question_id+'][]" aria-label="..." autocomplete="off" required><span class="input-group-addon"><a href="javascript:void(0)" class="remove_alternative"><span class="fa fa-times"></span></a></span><input type="hidden" name="alternative_type['+question_id+'][]" value="1"></div><!-- /input-group --><div class="form-group margin-top-10"><input type="file" class="x2files" name="alternative_image['+question_id+'][]" accept="image/*" autocomplete="off"></div></div>';
    }

    function get_multiple_alternative(alternative_id, question_id) {
    	return '<div class="alternative_'+alternative_id+' form-group"><div class="input-group"><span class="input-group-addon"><input type="checkbox" aria-label="..." disabled></span><input type="text" class="form-control" name="alternative_description['+question_id+'][]" aria-label="..." autocomplete="off" required><span class="input-group-addon"><a href="javascript:void(0)" class="remove_alternative"><span class="fa fa-times"></span></a></span><input type="hidden" name="alternative_type['+question_id+'][]" value="2"></div><!-- /input-group --><div class="form-group margin-top-10"><input type="file" class="x2files" name="alternative_image['+question_id+'][]" accept="image/*" autocomplete="off"></div></div>';
    }

    $("#newsurveyform").submit(function( event ) {

        //aca la validacion
        image_url_is_valid = validateImageURL();
        age_is_valid = validateAgeConstraint();

        if( ! image_url_is_valid ) {
            event.preventDefault();
            return false;
        }

        if( ! age_is_valid ) {
            event.preventDefault();
            return false;
        }

    	if( total_questions == 0 || total_alternatives == 0) {
    		$.smkAlert({text:'La encuesta debe de tener preguntas con alternativas.', type:'warning'});
    		event.preventDefault();
            return false;
    	}

	    if( !$('#newsurveyform').smkValidate({lang:'es'}) ){
			event.preventDefault();
            return false;
		}

	});

    $('input#survey_title').on("blur", function() {
        if ($.trim( $(this).val() ) == "") {
            $(this).val("Encuesta sin título");
        }
    });

    $(function () {
        var date_format = {
                    format: 'YYYY-MM-DD',
                    minDate: new Date()
                };
        $('#start_date').datetimepicker(date_format);
        $('#end_date').datetimepicker(date_format);
        /*
        $('#start_date').data("DateTimePicker").clear();
        $('#end_date').data("DateTimePicker").clear();
        */

        // $("#start_date").on("dp.change", function (e) {
        //     if ( $('#end_date').data("DateTimePicker").date() === null ) {
        //         $('#end_date').data("DateTimePicker").minDate(e.date).clear();
        //     }else{
        //         $('#end_date').data("DateTimePicker").minDate(e.date);                
        //     }
        // });
        // $("#end_date").on("dp.change", function (e) {
        //     $('#start_date').data("DateTimePicker").maxDate(e.date);
        // });
    });

    max_completed_per_survey.TouchSpin({
        min: 1,
        max: max_completed_surveys,
        step: 10,
        boostat: 10,
        maxboostedstep: 100,
        forcestepdivisibility: 'none'
    });

    $("input[name='age_comparison_value']").TouchSpin({
        min: 1,
        max: 100,
        step: 1,
        boostat: 10,
        maxboostedstep: 5,
        forcestepdivisibility: 'none'
    });

    $("input[name='age_min']").TouchSpin({
        min: 1,
        max: 99,
        step: 1,
        boostat: 10,
        maxboostedstep: 5,
        forcestepdivisibility: 'none'
    });

    $("input[name='age_max']").TouchSpin({
        min: 1,
        max: 100,
        step: 1,
        boostat: 10,
        maxboostedstep: 5,
        forcestepdivisibility: 'none'
    });

    function validateAgeConstraint() {

        age_constraint_type = $("input[name='age_constraint_type']:checked").val();

        age_enabled = $("input[name='enable_age_constraints']:checked").val();

        if(age_enabled == "1" && age_constraint_type == "2") {
        console.log('entro en la condicional');

            min_age = $('#age_min').val();
            max_age = $('#age_max').val();
        console.log('la edad minima es '+min_age+' y la edad maxima es '+max_age);

            if(max_age <= min_age ) {
                $.smkAlert({text:'El rango de edad no es coherente.', type:'warning'});
                return false;
            }
            return true;

        }
        return true;
    }

    function enableAgeConstraints(booleanx) {
        $( "#age_comparison" ).prop( "disabled", booleanx );
        $( "#age_comparison_value" ).prop( "disabled", booleanx );
        $( "#age_min" ).prop( "disabled", booleanx );
        $( "#age_max" ).prop( "disabled", booleanx );
        $("input[name='age_constraint_type']").prop( "disabled", booleanx );
    }


    function clickToEnableAgeConstraits() {
        age_enabled = $("input[name='enable_age_constraints']:checked").val();
        if(age_enabled == "1") {
            enableAgeConstraints(false);
        }else {
            enableAgeConstraints(true);
        }

    }

    function validateImageURL() {
        image_type = $("input[name='image_type']:checked").val();
        image_url = $("#image_url").val();
        if(image_type == "url") {
            if (image_url.trim()) {
                if(!IsValidImageUrl(image_url)) {
                    $.smkAlert({text:'La URL citada no es una imagen.', type:'warning'});
                    return false;
                }
            }
        }
        return true;
    }

    $(document).on('click', '#enable_age_constraints', function(e) {
        clickToEnableAgeConstraits();
    });

    $(document).on('change', "input[name=image_type]:radio", function(e) {
        image_type = $("input[name='image_type']:checked").val();
        if(image_type == "url") {
            $("#image_file").wrap('<form>').closest('form').get(0).reset();
            $("#image_file").unwrap();
            $("#image_file").prop( "disabled", true );
        }else{
            $("#image_file").prop( "disabled", false );
        }
    });

    function IsValidImageUrl(url) {
        var obj = new Image();
        obj.src = url;

        if (obj.complete) {
            return true;
        } else {
            return false;
        }
    }

});