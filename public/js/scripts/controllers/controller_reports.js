'use strict';


app.controller("appController", function($scope, $http, dataResource, sweet) {

  $scope.survey = [];
  $scope.data = [];
  $scope.alias = [];
  $scope.question_smalls = [];
  $scope.$watch('survey_id', function(value){
    $scope.id_template= value;
    if ($scope.id_template) {
      $http.get(base_url +'/admin/reports/survey/graphic-a/' + $scope.id_template + '?format=json')
        .success(function(res) {
          $scope.titulo = res.data.title;
          $scope.series = res.data.series;
          $scope.question_smalls = res.data.labels; 
          $scope.data = res.data.data;
          for (var i = 1; i <= res.data.labels.length; i++) {
            $scope.alias.push('Pregunta '+i);
          };
          $scope.labels = $scope.alias; 
            console.log($scope.alias);
        });   

    };  
    
  });

         // $scope.labels = ["January", "February", "March", "April", "May", "June", "July"];

        $scope.onClick = function (points, evt) {
          console.log(points, evt);
        };

});