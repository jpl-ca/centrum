'use strict';


app.controller("appController", function($scope, $http, dataResource, sweet,$location, $interval) {
  var params = window.location.search.split('?tab=')[1];

  if (params == undefined || params == '' ) {
    $scope.url = 'e';
  }else{
    $scope.url = params;
  };

  $scope.titulo = 'Mis Encuestas';

	$scope.planti= '-';
	$scope.group= '-';
	$scope.survey = {
		survey_state_id: 2, 
		survey_type_id: 1
	};

  $http.get(base_url +'/admin/surveys?format=json&fields=group').then(function(data){
    $scope.surveys = data.data.data;
    console.log($scope.surveys);
  },function(error){   
  });   
	
	$http.get(base_url +'/admin/survey-templates').then(function(data){
		$scope.plantillas = data.data.data;
	},function(error){
	});

   	$http.get(base_url +'/admin/groups').then(function(data){
		$scope.groups = data.data.data.data;
	},function(error){
	});

   	$scope.$watch('selected_template', function(plantilla){
   			if(plantilla){
   				$scope.survey.survey_template_id = plantilla.id;
          $scope.chk = "true";   				
   			}   			
   	});

   	$scope.$watch('selected_group', function(grupo){
   			if(grupo){
   				$scope.survey.group_id = grupo.id;
          $scope.chk2 = "true";  
   			}   			
   	});

   	$scope.$watch('survey.start_date', function(value){
   		if (value) {
   			$scope.survey.start_date = value;   			
   		};
   	});

   	$scope.add_survey = function(){
        if ($scope.chk == "true" && $scope.chk2 == "true" ) {
          $http.post(base_url +'/admin/surveys/copy-template', $scope.survey)      
            .success(function(res){
              sweet.show('', 'Encuesta registrada', 'success');  
              $scope.limpiar(); 
            })
            .error(function(res){
              });
        }else{
          sweet.show('', 'Tiene que completar todos los campos', 'error'); 
        }
   		 
   	}

      $scope.limpiar = function(){
        $scope.selected_group = '';
        $scope.selected_template = '';
        $scope.survey.start_date = f.getFullYear() + "-"  + (f.getMonth() +1) + "-" + f.getDate();
      }
      
	     var f = new Date();
          $scope.survey.start_date = f.getFullYear() + "-"  + (f.getMonth() +1) + "-" + f.getDate();


       $scope.delete_template = function(id, index){
        sweet.show({
             title: "",
             text: "¿Seguro que desea eliminar esta plantilla?",
             type: "warning",
             showCancelButton: true,
             confirmButtonColor: "#DD6B55",
             confirmButtonText: "Si, eliminar plantilla!",
             cancelButtonText: "No, cancelar!",
             closeOnConfirm: true,
             closeOnCancel: true }, 
          function(isConfirm){ 
             if (isConfirm) {
                $http.delete(base_url +'/admin/survey-templates/' + id)
                .success(function(res) {
                  // sweet.show("Eliminado!", "La plantilla fue eliminada", "success");
                  $scope.plantillas.splice(index, 1);
                       
                })
                .error(function(res) {
                        
                });
          } else {
                 
             }
          });          
       }

       $scope.delete_survey = function(id, index){
          sweet.show({
             title: "",
             text: "¿Seguro que desea eliminar esta encuesta?",
             type: "warning",
             showCancelButton: true,
             confirmButtonColor: "#DD6B55",
             confirmButtonText: "Si, eliminar encuesta!",
             cancelButtonText: "No, cancelar!",
             closeOnConfirm: true,
             closeOnCancel: true }, 
          function(isConfirm){ 
             if (isConfirm) {
                $http.delete(base_url +'/admin/surveys/' + id)
                .success(function(res) {
                  // sweet.show("Eliminado!", "La encuesta fue eliminada", "success");
                  $scope.surveys.splice(index, 1);
                       
                })
                .error(function(res) {
                        
                });                
             } else {
               
             }
          });          
       };

       $scope.activa = function(id){
        $scope.id_survey = id;
          $http.get(base_url +'/mis-encuestas/activar/' + $scope.id_survey)
          .success(function() {
            $http.get(base_url +'/admin/surveys?format=json&fields=group').then(function(data){
              $scope.surveys = data.data.data;
            },function(error){   
            });   
          }); 
       }

       $scope.desactiva = function(id){
        $scope.id_survey = id;
          $http.get(base_url +'/mis-encuestas/desactivar/' + $scope.id_survey)
          .success(function() {
            $http.get(base_url +'/admin/surveys?format=json&fields=group').then(function(data){
              $scope.surveys = data.data.data;
            },function(error){   
            });   
          }); 
       }


        

      


 	 
    $scope.placement = {
    options: [
      'top',
      'top-left',
      'top-right',
      'bottom',
      'bottom-left',
      'bottom-right',
      'left',
      'left-top',
      'left-bottom',
      'right',
      'right-top',
      'right-bottom'
    ],
    selected: 'top'
  };



     

});



