'use strict';


app.controller("appController", function($scope, $http, dataResource, sweet, $timeout) {
	
	
	$scope.template = new Object();
  $scope.template.sections = [];
	$scope.$watch('survey_id', function(value){
		$scope.id_template= value;
		if ($scope.id_template) {
			$http.get(base_url +'/admin/surveys/' + $scope.id_template).success(function(data) {
				$scope.template = data.data;	
        console.log($scope.template);
			});		
		};	

	});


	 $scope.Add_Section = function() {
    $scope.template.sections.push({
      questions:[],
      name: $scope.name
      });
  }

  $scope.Remove_Section = function(index) {
    $scope.template.sections.splice(index);
  }

  $scope.Add_Question = function(index) {
    var questions = new Object();
    var new_question = new Object();
    new_question.question_type_id = '1';
    new_question.weight = '10';
    new_question.mandatory = '1';
    new_question.alternatives = $scope.default_alternatives();    
    $scope.template.sections[index].questions.push(new_question);

    
  }

  $scope.Remove_Question = function(parentIndex,index) {
    $scope.template.sections[parentIndex].questions.splice(index, 1);
  }


  $scope.add_template = function() {
        $scope.template.end_date = '17-03-2016';
        $scope.template.start_date = '17-03-2016';
        $http.put(base_url +'/admin/survey-templates/' + $scope.id_template , $scope.template)
              .success(function(res){
                // sweet.show('', 'Plantilla Actualizada', 'success'); 
                $timeout ($scope.redirect, 500);   
              })
              .error(function(res){
              });                 
  }

    $scope.redirect = function() {
      window.location = base_url + "/mis-encuestas?tab=p";  
    }

  $scope.default_alternatives = function(){
    var array = [
      {
        alternative_type_id: "1",
        description: "Nunca",
        value: 1
      },
      {
        alternative_type_id: "1",
        description: "Pocas veces",
        value: 2
      },
      {
        alternative_type_id: "1",
        description: "Algunas veces",
        value: 3
      },
      {
        alternative_type_id: "1",
        description: "Casi siempre",
        value: 4
      },
      {
        alternative_type_id: "1",
        description: "Siempre",
        value: 5
      }
    ];
    return array;
  }




})