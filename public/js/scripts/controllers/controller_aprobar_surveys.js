'use strict';


app.controller("appController", function($scope, $http, dataResource,sweet, $timeout) {
	$scope.question_added = [];

	$scope.$watch('survey_id', function(value){
    $scope.id_survey= value;
    if ($scope.id_survey) {
      $http.get(base_url +'/admin/surveys/evaluate/'+$scope.id_survey)
      .success(function(data) {
        $scope.survey = data.data;  
      })
      .error(function(data) {  
      });

    };  
  })


   $scope.update = function(answer, isInitPhase) {
    if (isInitPhase) {
      if (answer.enabled == 1) {
        
      }
    } else {
      if (answer.enabled == 1) {
        
      } else {
        
      }
    } 
  };

  $scope.update_survey = function(){
    $http.post(base_url +'/admin/surveys/evaluate', $scope.survey)
              .success(function(res){
                // sweet.show('', 'Registros Actualizados', 'success');
                $timeout ($scope.redirect, 500);   
              })
              .error(function(res){
              });
  }

  $scope.redirect = function() {
    window.location="{{ route('encuestas.index') }}";  
  }
   

})