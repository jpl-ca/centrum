'use strict';


app.controller("appController", function($scope, $http, dataResource, sweet, $timeout) {

	$scope.planti= '-';
	$scope.group= '-';
	$scope.survey = {
		survey_state_id: 2, 
		survey_type_id: 1
	};

  $scope.$watch('survey_id', function(value){
    $scope.id_template= value;
    if ($scope.id_template) {
      $http.get(base_url +'/admin/surveys/' + $scope.id_template + '?fields=group').success(function(data) {
        $scope.survey = data.data; 
      });   
    };  

  });


       $scope.add_survey = function(){
          $http.put(base_url +'/admin/surveys/' + $scope.id_template, $scope.survey)      
            .success(function(res){
              // sweet.show('', 'Encuesta actualizada', 'success');  
              $timeout ($scope.redirect, 500);   
            })
            .error(function(res){
              });      
     }

     $scope.redirect = function() {
      window.location="../";  
    }

});