'use strict';


app.controller("appController", function($scope, $http, sweet) {

  $scope.contador = 0;
  $scope.items = [];
  var template = new Object();

  $scope.Add_Section = function() {
    $scope.items.push({
      questions:[],
      name: $scope.name
      });
  }

  $scope.Remove_Section = function(index) {
    $scope.items.splice(index);
  }

  $scope.Add_Question = function(index) {
    var questions = new Object();
    var new_question = new Object();
    new_question.question_type_id = '1';
    new_question.mandatory = '1';
    new_question.alternatives = $scope.default_alternatives();    
    $scope.items[index].questions.push(new_question);

    
  }

  $scope.Remove_Question = function(parentIndex,index) {
    $scope.items[parentIndex].questions.splice(index, 1);
  }


  $scope.add_template = function() {
        $scope.template.sections = $scope.items;
        $scope.template.end_date = '17-03-2016';
        $scope.template.start_date = '17-03-2016';
        $http.post(base_url +'/admin/survey-templates', $scope.template)
              .success(function(res){
                 sweet.show('', "Plantilla " + res.data.title + " registrada", 'success');   
                $scope.limpiar();
              })
              .error(function(res){
              });                  
  }

  $scope.limpiar = function(){
    $scope.template.title = '';
    $scope.template.description = '';
    $scope.items = [];
  }

  $scope.default_alternatives = function(){
    var array = [
      {
        alternative_type_id: "1",
        description: "Nunca",
        value: 1
      },
      {
        alternative_type_id: "1",
        description: "Pocas veces",
        value: 2
      },
      {
        alternative_type_id: "1",
        description: "Algunas veces",
        value: 3
      },
      {
        alternative_type_id: "1",
        description: "Casi siempre",
        value: 4
      },
      {
        alternative_type_id: "1",
        description: "Siempre",
        value: 5
      }
    ];
    return array;
  }


})