
var base_url=location.protocol+'//'+location.hostname+(location.port ? ':'+location.port: '');
var app = angular.module("app", ["ngResource", "ui.bootstrap", "ui.bootstrap.datetimepicker", "jkuri.checkbox", "hSweetAlert", "chart.js"]);
 
	

	app.factory("dataResource", function ($resource) {

		

	    return $resource(base_url + '/admin/survey-templates',
	        { 
	        	get: 
	        	{ 
	        		method: "GET", 
	        		isArray: true 
	        	}
	    })
	     return $resource(base_url + '/admin/groups',
	        { 
	        	get: 
	        	{ 
	        		method: "GET", 
	        		isArray: true 
	        	}
	    })
	     return $resource(base_url + '/admin/surveys?format=json&fields=group',
	        { 
	        	get: 
	        	{ 
	        		method: "GET", 
	        		isArray: true 
	        	}
	    })
	     
	})

	app.filter('date_format', function($filter)
  {
       return function(input)
       {
        if(input == null){ return ""; } 
       	input = input.split("-");
       	if(input.length==3){
       		var _date = input[2] + "/" + input[1] + "/" + input[0];	
       		return _date;
       	}
       };
  });