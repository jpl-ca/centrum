$(document).ready(function() {
    enableAgeConstraints(true);
	total_questions = 0;
	total_alternatives = 0;

    var max_fields      = 9999; //maximum input boxes allowed
    //var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var questions_wrapper         = $(".questions_wrapper"); //Fields wrapper
    //var add_button      = $(".add_alternative_button"); //Add button ID
    
    var x = 1; //initlal text box count
    var question_ider = 0; //initlal text box count

    var max_completed_per_survey = $("input[name='max_completed_per_survey']");
    var max_completed_surveys = max_completed_per_survey.attr('max');

    $("#newsurveyform").submit(function( event ) {

        //aca la validacion
        age_is_valid = validateAgeConstraint();

        if( ! age_is_valid ) {
            event.preventDefault();
            return false;
        }

	    if( !$('#newsurveyform').smkValidate({lang:'es'}) ){
			event.preventDefault();
            return false;
		}

	});

    $('input#survey_title').on("blur", function() {
        if ($.trim( $(this).val() ) == "") {
            $(this).val("Encuesta sin título");
        }
    });

    $(function () {
        var date_format = {
                    format: 'YYYY-MM-DD',
                    minDate: new Date()
                };
        $('#start_date').datetimepicker(date_format);
        $('#end_date').datetimepicker(date_format);
        /*
        $('#start_date').data("DateTimePicker").clear();
        $('#end_date').data("DateTimePicker").clear();
        */

        $("#start_date").on("dp.change", function (e) {
            if ( $('#end_date').data("DateTimePicker").date() === null ) {
                $('#end_date').data("DateTimePicker").minDate(e.date).clear();
            }else{
                $('#end_date').data("DateTimePicker").minDate(e.date);                
            }
        });
        $("#end_date").on("dp.change", function (e) {
            $('#start_date').data("DateTimePicker").maxDate(e.date);
        });
    });

    max_completed_per_survey.TouchSpin({
        min: 1,
        max: max_completed_surveys,
        step: 10,
        boostat: 10,
        maxboostedstep: 100,
        forcestepdivisibility: 'none'
    });

    $("input[name='age_comparison_value']").TouchSpin({
        min: 1,
        max: 100,
        step: 1,
        boostat: 10,
        maxboostedstep: 5,
        forcestepdivisibility: 'none'
    });

    $("input[name='age_min']").TouchSpin({
        min: 1,
        max: 99,
        step: 1,
        boostat: 10,
        maxboostedstep: 5,
        forcestepdivisibility: 'none'
    });

    $("input[name='age_max']").TouchSpin({
        min: 1,
        max: 100,
        step: 1,
        boostat: 10,
        maxboostedstep: 5,
        forcestepdivisibility: 'none'
    });

    function validateAgeConstraint() {

        age_constraint_type = $("input[name='age_constraint_type']:checked").val();

        age_enabled = $("input[name='enable_age_constraints']:checked").val();

        if(age_enabled == "1" && age_constraint_type == "2") {
        console.log('entro en la condicional');

            min_age = $('#age_min').val();
            max_age = $('#age_max').val();
        console.log('la edad minima es '+min_age+' y la edad maxima es '+max_age);

            if(max_age <= min_age ) {
                $.smkAlert({text:'El rango de edad no es coherente.', type:'warning'});
                return false;
            }
            return true;

        }
        return true;
    }

    function enableAgeConstraints(booleanx) {
        $( "#age_comparison" ).prop( "disabled", booleanx );
        $( "#age_comparison_value" ).prop( "disabled", booleanx );
        $( "#age_min" ).prop( "disabled", booleanx );
        $( "#age_max" ).prop( "disabled", booleanx );
        $("input[name='age_constraint_type']").prop( "disabled", booleanx );
    }


    function clickToEnableAgeConstraits() {
        age_enabled = $("input[name='enable_age_constraints']:checked").val();
        if(age_enabled == "1") {
            enableAgeConstraints(false);
        }else {
            enableAgeConstraints(true);
        }

    }
    clickToEnableAgeConstraits();
    $(document).on('click', '#enable_age_constraints', function(e) {
        clickToEnableAgeConstraits();
    });

});